import React from 'react';
import './App.css';

export class Shop extends React.Component {
    render() {
        return (
            <div classname='Shop'>
<div className="shop-main-wrapper section-padding">
  <div className="container">
    <div className="row">
      {/* sidebar area start */}
      <div className="col-lg-3 order-2 order-lg-1">
        <aside className="sidebar-wrapper">
          {/* single sidebar start */}
          <div className="sidebar-single">
            <h5 className="sidebar-title">categories</h5>
            <div className="sidebar-body">
              <ul className="shop-categories">
                <li>
                  <a href="#">
                    fashionware <span>(10)</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    kitchenware <span>(5)</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    electronics <span>(8)</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    accessories <span>(4)</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    shoe <span>(5)</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    toys <span>(2)</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          {/* single sidebar end */}
          {/* single sidebar start */}
          <div className="sidebar-single">
            <h5 className="sidebar-title">price</h5>
            <div className="sidebar-body">
              <div className="price-range-wrap">
                <div className="price-range" data-min={1} data-max={1000} />
                <div className="range-slider">
                  <form
                    action="#"
                    className="d-flex align-items-center justify-content-between"
                  >
                    <div className="price-input">
                      <label htmlFor="amount">Price: </label>
                      <input type="text" id="amount" />
                    </div>
                    <button className="filter-btn">filter</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
          {/* single sidebar end */}
          {/* single sidebar start */}
          <div className="sidebar-single">
            <h5 className="sidebar-title">Brand</h5>
            <div className="sidebar-body">
              <ul className="checkbox-container categories-list">
                <li>
                  <div className="custom-control custom-checkbox">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="customCheck2"
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="customCheck2"
                    >
                      Studio (3)
                    </label>
                  </div>
                </li>
                <li>
                  <div className="custom-control custom-checkbox">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="customCheck3"
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="customCheck3"
                    >
                      Hastech (4)
                    </label>
                  </div>
                </li>
                <li>
                  <div className="custom-control custom-checkbox">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="customCheck4"
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="customCheck4"
                    >
                      Quickiin (15)
                    </label>
                  </div>
                </li>
                <li>
                  <div className="custom-control custom-checkbox">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="customCheck1"
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="customCheck1"
                    >
                      Graphic corner (10)
                    </label>
                  </div>
                </li>
                <li>
                  <div className="custom-control custom-checkbox">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="customCheck5"
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="customCheck5"
                    >
                      devItems (12)
                    </label>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          {/* single sidebar end */}
          {/* single sidebar start */}
          <div className="sidebar-single">
            <h5 className="sidebar-title">color</h5>
            <div className="sidebar-body">
              <ul className="checkbox-container categories-list">
                <li>
                  <div className="custom-control custom-checkbox">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="customCheck12"
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="customCheck12"
                    >
                      black (20)
                    </label>
                  </div>
                </li>
                <li>
                  <div className="custom-control custom-checkbox">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="customCheck13"
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="customCheck13"
                    >
                      red (6)
                    </label>
                  </div>
                </li>
                <li>
                  <div className="custom-control custom-checkbox">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="customCheck14"
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="customCheck14"
                    >
                      blue (8)
                    </label>
                  </div>
                </li>
                <li>
                  <div className="custom-control custom-checkbox">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="customCheck11"
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="customCheck11"
                    >
                      green (5)
                    </label>
                  </div>
                </li>
                <li>
                  <div className="custom-control custom-checkbox">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="customCheck15"
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="customCheck15"
                    >
                      pink (4)
                    </label>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          {/* single sidebar end */}
          {/* single sidebar start */}
          <div className="sidebar-single">
            <h5 className="sidebar-title">size</h5>
            <div className="sidebar-body">
              <ul className="checkbox-container categories-list">
                <li>
                  <div className="custom-control custom-checkbox">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="customCheck111"
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="customCheck111"
                    >
                      S (4)
                    </label>
                  </div>
                </li>
                <li>
                  <div className="custom-control custom-checkbox">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="customCheck222"
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="customCheck222"
                    >
                      M (5)
                    </label>
                  </div>
                </li>
                <li>
                  <div className="custom-control custom-checkbox">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="customCheck333"
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="customCheck333"
                    >
                      L (7)
                    </label>
                  </div>
                </li>
                <li>
                  <div className="custom-control custom-checkbox">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="customCheck444"
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="customCheck444"
                    >
                      XL (3)
                    </label>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          {/* single sidebar end */}
          {/* single sidebar start */}
          <div className="sidebar-banner">
            <div className="img-container">
              <a href="#">
                <img src="img/banner/sidebar-banner.jpg" alt />
              </a>
            </div>
          </div>
          {/* single sidebar end */}
        </aside>
      </div>
      {/* sidebar area end */}
      {/* shop main wrapper start */}
      <div className="col-lg-9 order-1 order-lg-2">
        <div className="shop-product-wrapper">
          {/* shop product top wrap start */}
          <div className="shop-top-bar">
            <div className="row align-items-center">
              <div className="col-lg-7 col-md-6 order-2 order-md-1">
                <div className="top-bar-left">
                  <div className="product-view-mode">
                    <a
                      className="active"
                      href="#"
                      data-target="grid-view"
                      data-toggle="tooltip"
                      title="Grid View"
                    >
                      <i className="fa fa-th" />
                    </a>
                    <a
                      href="#"
                      data-target="list-view"
                      data-toggle="tooltip"
                      title="List View"
                    >
                      <i className="fa fa-list" />
                    </a>
                  </div>
                  <div className="product-amount">
                    <p>Showing 1–16 of 21 results</p>
                  </div>
                </div>
              </div>
              <div className="col-lg-5 col-md-6 order-1 order-md-2">
                <div className="top-bar-right">
                  <div className="product-short">
                    <p>Sort By : </p>
                    <select className="nice-select" name="sortby">
                      <option value="trending">Relevance</option>
                      <option value="sales">Name (A - Z)</option>
                      <option value="sales">Name (Z - A)</option>
                      <option value="rating">Price (Low &gt; High)</option>
                      <option value="date">Rating (Lowest)</option>
                      <option value="price-asc">Model (A - Z)</option>
                      <option value="price-asc">Model (Z - A)</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* shop product top wrap start */}
          {/* product item list wrapper start */}
          <div className="shop-product-wrap grid-view row mbn-30">
            {/* product single item start */}
            <div className="col-md-4 col-sm-6">
              {/* product grid start */}
              <div className="product-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-1.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-18.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-caption text-center">
                  <div className="product-identity">
                    <p className="manufacturer-name">
                      <a href="product-details.html">Platinum</a>
                    </p>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h6 className="product-name">
                    <a href="product-details.html">Perfect Diamond Jewelry</a>
                  </h6>
                  <div className="price-box">
                    <span className="price-regular">$60.00</span>
                    <span className="price-old">
                      <del>$70.00</del>
                    </span>
                  </div>
                </div>
              </div>
              {/* product grid end */}
              {/* product list item end */}
              <div className="product-list-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-1.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-18.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-content-list">
                  <div className="manufacturer-name">
                    <a href="product-details.html">Platinum</a>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h5 className="product-name">
                    <a href="product-details.html">Perfect Diamond Jewelry</a>
                  </h5>
                  <div className="price-box">
                    <span className="price-regular">$50.00</span>
                    <span className="price-old">
                      <del>$29.99</del>
                    </span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    Unde perspiciatis quod numquam, sit fugiat, deserunt ipsa
                    mollitia sunt quam corporis ullam rem, accusantium adipisci
                    officia eaque.
                  </p>
                </div>
              </div>
              {/* product list item end */}
            </div>
            {/* product single item start */}
            {/* product single item start */}
            <div className="col-md-4 col-sm-6">
              {/* product grid start */}
              <div className="product-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-2.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-17.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-caption text-center">
                  <div className="product-identity">
                    <p className="manufacturer-name">
                      <a href="product-details.html">Quickiin</a>
                    </p>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h6 className="product-name">
                    <a href="product-details.html">Handmade Golden Necklace</a>
                  </h6>
                  <div className="price-box">
                    <span className="price-regular">$60.00</span>
                    <span className="price-old">
                      <del>$70.00</del>
                    </span>
                  </div>
                </div>
              </div>
              {/* product grid end */}
              {/* product list item end */}
              <div className="product-list-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-2.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-17.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-content-list">
                  <div className="manufacturer-name">
                    <a href="product-details.html">Diamond</a>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h5 className="product-name">
                    <a href="product-details.html">Handmade Golden Necklace</a>
                  </h5>
                  <div className="price-box">
                    <span className="price-regular">$50.00</span>
                    <span className="price-old">
                      <del>$29.99</del>
                    </span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    Unde perspiciatis quod numquam, sit fugiat, deserunt ipsa
                    mollitia sunt quam corporis ullam rem, accusantium adipisci
                    officia eaque.
                  </p>
                </div>
              </div>
              {/* product list item end */}
            </div>
            {/* product single item start */}
            {/* product single item start */}
            <div className="col-md-4 col-sm-6">
              {/* product grid start */}
              <div className="product-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-3.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-16.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-caption text-center">
                  <div className="product-identity">
                    <p className="manufacturer-name">
                      <a href="product-details.html">Quickiin</a>
                    </p>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h6 className="product-name">
                    <a href="product-details.html">
                      Diamond Exclusive Ornament
                    </a>
                  </h6>
                  <div className="price-box">
                    <span className="price-regular">$60.00</span>
                    <span className="price-old">
                      <del>$70.00</del>
                    </span>
                  </div>
                </div>
              </div>
              {/* product grid end */}
              {/* product list item end */}
              <div className="product-list-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-3.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-16.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-content-list">
                  <div className="manufacturer-name">
                    <a href="product-details.html">Quickiin</a>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h5 className="product-name">
                    <a href="product-details.html">
                      Diamond Exclusive Ornament
                    </a>
                  </h5>
                  <div className="price-box">
                    <span className="price-regular">$50.00</span>
                    <span className="price-old">
                      <del>$29.99</del>
                    </span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    Unde perspiciatis quod numquam, sit fugiat, deserunt ipsa
                    mollitia sunt quam corporis ullam rem, accusantium adipisci
                    officia eaque.
                  </p>
                </div>
              </div>
              {/* product list item end */}
            </div>
            {/* product single item start */}
            {/* product single item start */}
            <div className="col-md-4 col-sm-6">
              {/* product grid start */}
              <div className="product-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-4.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-15.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-caption text-center">
                  <div className="product-identity">
                    <p className="manufacturer-name">
                      <a href="product-details.html">BDevs</a>
                    </p>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h6 className="product-name">
                    <a href="product-details.html">Handmade Golden Necklace</a>
                  </h6>
                  <div className="price-box">
                    <span className="price-regular">$60.00</span>
                    <span className="price-old">
                      <del>$70.00</del>
                    </span>
                  </div>
                </div>
              </div>
              {/* product grid end */}
              {/* product list item end */}
              <div className="product-list-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-4.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-15.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-content-list">
                  <div className="manufacturer-name">
                    <a href="product-details.html">BDevs</a>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h5 className="product-name">
                    <a href="product-details.html">Handmade Golden Necklace</a>
                  </h5>
                  <div className="price-box">
                    <span className="price-regular">$50.00</span>
                    <span className="price-old">
                      <del>$29.99</del>
                    </span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    Unde perspiciatis quod numquam, sit fugiat, deserunt ipsa
                    mollitia sunt quam corporis ullam rem, accusantium adipisci
                    officia eaque.
                  </p>
                </div>
              </div>
              {/* product list item end */}
            </div>
            {/* product single item start */}
            {/* product single item start */}
            <div className="col-md-4 col-sm-6">
              {/* product grid start */}
              <div className="product-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-5.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-14.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-caption text-center">
                  <div className="product-identity">
                    <p className="manufacturer-name">
                      <a href="product-details.html">Hastech</a>
                    </p>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h6 className="product-name">
                    <a href="product-details.html">Perfect Diamond Jewelry</a>
                  </h6>
                  <div className="price-box">
                    <span className="price-regular">$60.00</span>
                    <span className="price-old">
                      <del>$70.00</del>
                    </span>
                  </div>
                </div>
              </div>
              {/* product grid end */}
              {/* product list item end */}
              <div className="product-list-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-5.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-14.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-content-list">
                  <div className="manufacturer-name">
                    <a href="product-details.html">HasTech</a>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h5 className="product-name">
                    <a href="product-details.html">Perfect Diamond Jewelry</a>
                  </h5>
                  <div className="price-box">
                    <span className="price-regular">$50.00</span>
                    <span className="price-old">
                      <del>$29.99</del>
                    </span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    Unde perspiciatis quod numquam, sit fugiat, deserunt ipsa
                    mollitia sunt quam corporis ullam rem, accusantium adipisci
                    officia eaque.
                  </p>
                </div>
              </div>
              {/* product list item end */}
            </div>
            {/* product single item start */}
            {/* product single item start */}
            <div className="col-md-4 col-sm-6">
              {/* product grid start */}
              <div className="product-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-6.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-13.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-caption text-center">
                  <div className="product-identity">
                    <p className="manufacturer-name">
                      <a href="product-details.html">Gold</a>
                    </p>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h6 className="product-name">
                    <a href="product-details.html">
                      Diamond Exclusive Ornament
                    </a>
                  </h6>
                  <div className="price-box">
                    <span className="price-regular">$60.00</span>
                    <span className="price-old">
                      <del>$70.00</del>
                    </span>
                  </div>
                </div>
              </div>
              {/* product grid end */}
              {/* product list item end */}
              <div className="product-list-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-6.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-13.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-content-list">
                  <div className="manufacturer-name">
                    <a href="product-details.html">Gold</a>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h5 className="product-name">
                    <a href="product-details.html">
                      Diamond Exclusive Ornament
                    </a>
                  </h5>
                  <div className="price-box">
                    <span className="price-regular">$50.00</span>
                    <span className="price-old">
                      <del>$29.99</del>
                    </span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    Unde perspiciatis quod numquam, sit fugiat, deserunt ipsa
                    mollitia sunt quam corporis ullam rem, accusantium adipisci
                    officia eaque.
                  </p>
                </div>
              </div>
              {/* product list item end */}
            </div>
            {/* product single item start */}
            {/* product single item start */}
            <div className="col-md-4 col-sm-6">
              {/* product grid start */}
              <div className="product-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-7.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-12.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-caption text-center">
                  <div className="product-identity">
                    <p className="manufacturer-name">
                      <a href="product-details.html">Silver</a>
                    </p>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h6 className="product-name">
                    <a href="product-details.html">Handmade Golden Necklace</a>
                  </h6>
                  <div className="price-box">
                    <span className="price-regular">$60.00</span>
                    <span className="price-old">
                      <del>$70.00</del>
                    </span>
                  </div>
                </div>
              </div>
              {/* product grid end */}
              {/* product list item end */}
              <div className="product-list-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-7.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-12.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-content-list">
                  <div className="manufacturer-name">
                    <a href="product-details.html">Silver</a>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h5 className="product-name">
                    <a href="product-details.html">Handmade Golden Necklace</a>
                  </h5>
                  <div className="price-box">
                    <span className="price-regular">$50.00</span>
                    <span className="price-old">
                      <del>$29.99</del>
                    </span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    Unde perspiciatis quod numquam, sit fugiat, deserunt ipsa
                    mollitia sunt quam corporis ullam rem, accusantium adipisci
                    officia eaque.
                  </p>
                </div>
              </div>
              {/* product list item end */}
            </div>
            {/* product single item start */}
            {/* product single item start */}
            <div className="col-md-4 col-sm-6">
              {/* product grid start */}
              <div className="product-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-8.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-11.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-caption text-center">
                  <div className="product-identity">
                    <p className="manufacturer-name">
                      <a href="product-details.html">Maya</a>
                    </p>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h6 className="product-name">
                    <a href="product-details.html">Perfect Diamond Jewelry</a>
                  </h6>
                  <div className="price-box">
                    <span className="price-regular">$60.00</span>
                    <span className="price-old">
                      <del>$70.00</del>
                    </span>
                  </div>
                </div>
              </div>
              {/* product grid end */}
              {/* product list item end */}
              <div className="product-list-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-8.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-11.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-content-list">
                  <div className="manufacturer-name">
                    <a href="product-details.html">Maya</a>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h5 className="product-name">
                    <a href="product-details.html">Perfect Diamond Jewelry</a>
                  </h5>
                  <div className="price-box">
                    <span className="price-regular">$50.00</span>
                    <span className="price-old">
                      <del>$29.99</del>
                    </span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    Unde perspiciatis quod numquam, sit fugiat, deserunt ipsa
                    mollitia sunt quam corporis ullam rem, accusantium adipisci
                    officia eaque.
                  </p>
                </div>
              </div>
              {/* product list item end */}
            </div>
            {/* product single item start */}
            {/* product single item start */}
            <div className="col-md-4 col-sm-6">
              {/* product grid start */}
              <div className="product-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-9.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-10.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-caption text-center">
                  <div className="product-identity">
                    <p className="manufacturer-name">
                      <a href="product-details.html">Quickiin</a>
                    </p>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h6 className="product-name">
                    <a href="product-details.html">Perfect Diamond Jewelry</a>
                  </h6>
                  <div className="price-box">
                    <span className="price-regular">$60.00</span>
                    <span className="price-old">
                      <del>$70.00</del>
                    </span>
                  </div>
                </div>
              </div>
              {/* product grid end */}
              {/* product list item end */}
              <div className="product-list-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-9.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-10.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-content-list">
                  <div className="manufacturer-name">
                    <a href="product-details.html">Quickiin</a>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h5 className="product-name">
                    <a href="product-details.html">Perfect Diamond Jewelry</a>
                  </h5>
                  <div className="price-box">
                    <span className="price-regular">$50.00</span>
                    <span className="price-old">
                      <del>$29.99</del>
                    </span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    Unde perspiciatis quod numquam, sit fugiat, deserunt ipsa
                    mollitia sunt quam corporis ullam rem, accusantium adipisci
                    officia eaque.
                  </p>
                </div>
              </div>
              {/* product list item end */}
            </div>
            {/* product single item start */}
            {/* product single item start */}
            <div className="col-md-4 col-sm-6">
              {/* product grid start */}
              <div className="product-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-10.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-9.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-caption text-center">
                  <div className="product-identity">
                    <p className="manufacturer-name">
                      <a href="product-details.html">Jem</a>
                    </p>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h6 className="product-name">
                    <a href="product-details.html">Handmade Golden Necklace</a>
                  </h6>
                  <div className="price-box">
                    <span className="price-regular">$60.00</span>
                    <span className="price-old">
                      <del>$70.00</del>
                    </span>
                  </div>
                </div>
              </div>
              {/* product grid end */}
              {/* product list item end */}
              <div className="product-list-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-10.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-9.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-content-list">
                  <div className="manufacturer-name">
                    <a href="product-details.html">Jem</a>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h5 className="product-name">
                    <a href="product-details.html">Handmade Golden Necklace</a>
                  </h5>
                  <div className="price-box">
                    <span className="price-regular">$50.00</span>
                    <span className="price-old">
                      <del>$29.99</del>
                    </span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    Unde perspiciatis quod numquam, sit fugiat, deserunt ipsa
                    mollitia sunt quam corporis ullam rem, accusantium adipisci
                    officia eaque.
                  </p>
                </div>
              </div>
              {/* product list item end */}
            </div>
            {/* product single item start */}
            {/* product single item start */}
            <div className="col-md-4 col-sm-6">
              {/* product grid start */}
              <div className="product-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-11.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-8.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-caption text-center">
                  <div className="product-identity">
                    <p className="manufacturer-name">
                      <a href="product-details.html">Quickiin</a>
                    </p>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h6 className="product-name">
                    <a href="product-details.html">
                      Diamond Exclusive Ornament
                    </a>
                  </h6>
                  <div className="price-box">
                    <span className="price-regular">$60.00</span>
                    <span className="price-old">
                      <del>$70.00</del>
                    </span>
                  </div>
                </div>
              </div>
              {/* product grid end */}
              {/* product list item end */}
              <div className="product-list-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-11.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-8.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-content-list">
                  <div className="manufacturer-name">
                    <a href="product-details.html">Quickiin</a>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h5 className="product-name">
                    <a href="product-details.html">
                      Diamond Exclusive Ornament
                    </a>
                  </h5>
                  <div className="price-box">
                    <span className="price-regular">$50.00</span>
                    <span className="price-old">
                      <del>$29.99</del>
                    </span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    Unde perspiciatis quod numquam, sit fugiat, deserunt ipsa
                    mollitia sunt quam corporis ullam rem, accusantium adipisci
                    officia eaque.
                  </p>
                </div>
              </div>
              {/* product list item end */}
            </div>
            {/* product single item start */}
            {/* product single item start */}
            <div className="col-md-4 col-sm-6">
              {/* product grid start */}
              <div className="product-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-12.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-7.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-caption text-center">
                  <div className="product-identity">
                    <p className="manufacturer-name">
                      <a href="product-details.html">HasTech</a>
                    </p>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h6 className="product-name">
                    <a href="product-details.html">Perfect Diamond Jewelry</a>
                  </h6>
                  <div className="price-box">
                    <span className="price-regular">$60.00</span>
                    <span className="price-old">
                      <del>$70.00</del>
                    </span>
                  </div>
                </div>
              </div>
              {/* product grid end */}
              {/* product list item end */}
              <div className="product-list-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-12.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-7.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-content-list">
                  <div className="manufacturer-name">
                    <a href="product-details.html">HasTech</a>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h5 className="product-name">
                    <a href="product-details.html">Perfect Diamond Jewelry</a>
                  </h5>
                  <div className="price-box">
                    <span className="price-regular">$50.00</span>
                    <span className="price-old">
                      <del>$29.99</del>
                    </span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    Unde perspiciatis quod numquam, sit fugiat, deserunt ipsa
                    mollitia sunt quam corporis ullam rem, accusantium adipisci
                    officia eaque.
                  </p>
                </div>
              </div>
              {/* product list item end */}
            </div>
            {/* product single item start */}
            {/* product single item start */}
            <div className="col-md-4 col-sm-6">
              {/* product grid start */}
              <div className="product-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-13.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-6.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-caption text-center">
                  <div className="product-identity">
                    <p className="manufacturer-name">
                      <a href="product-details.html">Gold</a>
                    </p>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h6 className="product-name">
                    <a href="product-details.html">Perfect Diamond Jewelry</a>
                  </h6>
                  <div className="price-box">
                    <span className="price-regular">$60.00</span>
                    <span className="price-old">
                      <del>$70.00</del>
                    </span>
                  </div>
                </div>
              </div>
              {/* product grid end */}
              {/* product list item end */}
              <div className="product-list-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-13.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-6.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-content-list">
                  <div className="manufacturer-name">
                    <a href="product-details.html">Gold</a>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h5 className="product-name">
                    <a href="product-details.html">Perfect Diamond Jewelry</a>
                  </h5>
                  <div className="price-box">
                    <span className="price-regular">$50.00</span>
                    <span className="price-old">
                      <del>$29.99</del>
                    </span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    Unde perspiciatis quod numquam, sit fugiat, deserunt ipsa
                    mollitia sunt quam corporis ullam rem, accusantium adipisci
                    officia eaque.
                  </p>
                </div>
              </div>
              {/* product list item end */}
            </div>
            {/* product single item start */}
            {/* product single item start */}
            <div className="col-md-4 col-sm-6">
              {/* product grid start */}
              <div className="product-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-14.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-5.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-caption text-center">
                  <div className="product-identity">
                    <p className="manufacturer-name">
                      <a href="product-details.html">Gold</a>
                    </p>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h6 className="product-name">
                    <a href="product-details.html">Handmade Golden Necklace</a>
                  </h6>
                  <div className="price-box">
                    <span className="price-regular">$60.00</span>
                    <span className="price-old">
                      <del>$70.00</del>
                    </span>
                  </div>
                </div>
              </div>
              {/* product grid end */}
              {/* product list item end */}
              <div className="product-list-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-14.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-5.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-content-list">
                  <div className="manufacturer-name">
                    <a href="product-details.html">Gold</a>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h5 className="product-name">
                    <a href="product-details.html">Handmade Golden Necklace</a>
                  </h5>
                  <div className="price-box">
                    <span className="price-regular">$50.00</span>
                    <span className="price-old">
                      <del>$29.99</del>
                    </span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    Unde perspiciatis quod numquam, sit fugiat, deserunt ipsa
                    mollitia sunt quam corporis ullam rem, accusantium adipisci
                    officia eaque.
                  </p>
                </div>
              </div>
              {/* product list item end */}
            </div>
            {/* product single item start */}
            {/* product single item start */}
            <div className="col-md-4 col-sm-6">
              {/* product grid start */}
              <div className="product-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-15.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-4.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-caption text-center">
                  <div className="product-identity">
                    <p className="manufacturer-name">
                      <a href="product-details.html">Quickiin</a>
                    </p>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h6 className="product-name">
                    <a href="product-details.html">
                      Diamond Exclusive Ornament
                    </a>
                  </h6>
                  <div className="price-box">
                    <span className="price-regular">$60.00</span>
                    <span className="price-old">
                      <del>$70.00</del>
                    </span>
                  </div>
                </div>
              </div>
              {/* product grid end */}
              {/* product list item end */}
              <div className="product-list-item">
                <figure className="product-thumb">
                  <a href="product-details.html">
                    <img
                      className="pri-img"
                      src="img/product/product-15.jpg"
                      alt="product"
                    />
                    <img
                      className="sec-img"
                      src="img/product/product-4.jpg"
                      alt="product"
                    />
                  </a>
                  <div className="product-badge">
                    <div className="product-label new">
                      <span>new</span>
                    </div>
                    <div className="product-label discount">
                      <span>10%</span>
                    </div>
                  </div>
                  <div className="button-group">
                    <a
                      href="wishlist.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to wishlist"
                    >
                      <i className="pe-7s-like" />
                    </a>
                    <a
                      href="compare.html"
                      data-toggle="tooltip"
                      data-placement="left"
                      title="Add to Compare"
                    >
                      <i className="pe-7s-refresh-2" />
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick_view">
                      <span
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Quick View"
                      >
                        <i className="pe-7s-search" />
                      </span>
                    </a>
                  </div>
                  <div className="cart-hover">
                    <button className="btn btn-cart">add to cart</button>
                  </div>
                </figure>
                <div className="product-content-list">
                  <div className="manufacturer-name">
                    <a href="product-details.html">Quickiin</a>
                  </div>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                  <h5 className="product-name">
                    <a href="product-details.html">
                      Diamond Exclusive Ornament
                    </a>
                  </h5>
                  <div className="price-box">
                    <span className="price-regular">$50.00</span>
                    <span className="price-old">
                      <del>$29.99</del>
                    </span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    Unde perspiciatis quod numquam, sit fugiat, deserunt ipsa
                    mollitia sunt quam corporis ullam rem, accusantium adipisci
                    officia eaque.
                  </p>
                </div>
              </div>
              {/* product list item end */}
            </div>
            {/* product single item start */}
          </div>
          {/* product item list wrapper end */}
          {/* start pagination area */}
          <div className="paginatoin-area text-center">
            <ul className="pagination-box">
              <li>
                <a className="previous" href="#">
                  <i className="pe-7s-angle-left" />
                </a>
              </li>
              <li className="active">
                <a href="#">1</a>
              </li>
              <li>
                <a href="#">2</a>
              </li>
              <li>
                <a href="#">3</a>
              </li>
              <li>
                <a className="next" href="#">
                  <i className="pe-7s-angle-right" />
                </a>
              </li>
            </ul>
          </div>
          {/* end pagination area */}
        </div>
      </div>
      {/* shop main wrapper end */}
    </div>
  </div>
</div>

            </div>
            );
    }
}

export default Shop;