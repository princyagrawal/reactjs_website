import React from 'react';
import './App.css';

export class Product extends React.Component {
    render() {
        return (
            <div classname='Product'>
<div className="shop-main-wrapper section-padding pb-0">
  <div className="container">
    <div className="row">
      {/* product details wrapper start */}
      <div className="col-lg-12 order-1 order-lg-2">
        {/* product details inner end */}
        <div className="product-details-inner">
          <div className="row">
            <div className="col-lg-5">
              <div className="product-large-slider">
                <div className="pro-large-img img-zoom">
                  <img
                    src="img/product/product-details-img1.jpg"
                    alt="product-details"
                  />
                </div>
               
              </div>
            
            </div>
            <div className="col-lg-7">
              <div className="product-details-des">
                <div className="manufacturer-name">
                  <a href="product-details.html">HasTech</a>
                </div>
                <h3 className="product-name">
                  Handmade Golden Necklace Full Family Package
                </h3>
                <div className="ratings d-flex">
                  <span>
                    <i className="fa fa-star-o" />
                  </span>
                  <span>
                    <i className="fa fa-star-o" />
                  </span>
                  <span>
                    <i className="fa fa-star-o" />
                  </span>
                  <span>
                    <i className="fa fa-star-o" />
                  </span>
                  <span>
                    <i className="fa fa-star-o" />
                  </span>
                  <div className="pro-review">
                    <span>1 Reviews</span>
                  </div>
                </div>
                <div className="price-box">
                  <span className="price-regular">$70.00</span>
                  <span className="price-old">
                    <del>$90.00</del>
                  </span>
                </div>
                <h5 className="offer-text">
                  <strong>Hurry up</strong>! offer ends in:
                </h5>
                <div
                  className="product-countdown"
                  data-countdown="2019/12/20"
                />
                <div className="availability">
                  <i className="fa fa-check-circle" />
                  <span>200 in stock</span>
                </div>
                <p className="pro-desc">
                  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                  diam nonumy eirmod tempor invidunt ut labore et dolore magna
                  aliquyam erat, sed diam voluptua. Phasellus id nisi quis justo
                  tempus mollis sed et dui. In hac habitasse platea dictumst.
                </p>
                <div className="quantity-cart-box d-flex align-items-center">
                  <h6 className="option-title">qty:</h6>
                  <div className="quantity">
                    <div className="pro-qty">
                      <input type="text" defaultValue={1} />
                    </div>
                  </div>
                  <div className="action_link">
                    <a className="btn btn-cart2" href="#">
                      Add to cart
                    </a>
                  </div>
                </div>
                <div className="pro-size">
                  <h6 className="option-title">size :</h6>
                  <select className="nice-select">
                    <option>S</option>
                    <option>M</option>
                    <option>L</option>
                    <option>XL</option>
                  </select>
                </div>
                <div className="color-option">
                  <h6 className="option-title">color :</h6>
                  <ul className="color-categories">
                    <li>
                      <a
                        className="c-lightblue"
                        href="#"
                        title="LightSteelblue"
                      />
                    </li>
                    <li>
                      <a className="c-darktan" href="#" title="Darktan" />
                    </li>
                    <li>
                      <a className="c-grey" href="#" title="Grey" />
                    </li>
                    <li>
                      <a className="c-brown" href="#" title="Brown" />
                    </li>
                  </ul>
                </div>
                <div className="useful-links">
                  <a href="#" data-toggle="tooltip" title="Compare">
                    <i className="pe-7s-refresh-2" />
                    compare
                  </a>
                  <a href="#" data-toggle="tooltip" title="Wishlist">
                    <i className="pe-7s-like" />
                    wishlist
                  </a>
                </div>
                <div className="like-icon">
                  <a className="facebook" href="#">
                    <i className="fa fa-facebook" />
                    like
                  </a>
                  <a className="twitter" href="#">
                    <i className="fa fa-twitter" />
                    tweet
                  </a>
                  <a className="pinterest" href="#">
                    <i className="fa fa-pinterest" />
                    save
                  </a>
                  <a className="google" href="#">
                    <i className="fa fa-google-plus" />
                    share
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* product details inner end */}
        {/* product details reviews start */}
        <div className="product-details-reviews section-padding pb-0">
          <div className="row">
            <div className="col-lg-12">
              <div className="product-review-info">
                <ul className="nav review-tab">
                  <li>
                    <a className="active" data-toggle="tab" href="#tab_one">
                      description
                    </a>
                  </li>
                  <li>
                    <a data-toggle="tab" href="#tab_two">
                      information
                    </a>
                  </li>
                  <li>
                    <a data-toggle="tab" href="#tab_three">
                      reviews (1)
                    </a>
                  </li>
                </ul>
                <div className="tab-content reviews-tab">
                  <div className="tab-pane fade show active" id="tab_one">
                    <div className="tab-one">
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Nam fringilla augue nec est tristique auctor. Ipsum
                        metus feugiat sem, quis fermentum turpis eros eget
                        velit. Donec ac tempus ante. Fusce ultricies massa
                        massa. Fusce aliquam, purus eget sagittis vulputate,
                        sapien libero hendrerit est, sed commodo augue nisi non
                        neque.Cras neque metus, consequat et blandit et, luctus
                        a nunc. Etiam gravida vehicula tellus, in imperdiet
                        ligula euismod eget. Pellentesque habitant morbi
                        tristique senectus et netus et malesuada fames ac turpis
                        egestas. Nam erat mi, rutrum at sollicitudin rhoncus
                      </p>
                    </div>
                  </div>
                  <div className="tab-pane fade" id="tab_two">
                    <table className="table table-bordered">
                      <tbody>
                        <tr>
                          <td>color</td>
                          <td>black, blue, red</td>
                        </tr>
                        <tr>
                          <td>size</td>
                          <td>L, M, S</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div className="tab-pane fade" id="tab_three">
                    <form action="#" className="review-form">
                      <h5>
                        1 review for <span>Chaz Kangeroo</span>
                      </h5>
                      <div className="total-reviews">
                        <div className="rev-avatar">
                          <img src="img/about/avatar.jpg" alt />
                        </div>
                        <div className="review-box">
                          <div className="ratings">
                            <span className="good">
                              <i className="fa fa-star" />
                            </span>
                            <span className="good">
                              <i className="fa fa-star" />
                            </span>
                            <span className="good">
                              <i className="fa fa-star" />
                            </span>
                            <span className="good">
                              <i className="fa fa-star" />
                            </span>
                            <span>
                              <i className="fa fa-star" />
                            </span>
                          </div>
                          <div className="post-author">
                            <p>
                              <span>admin -</span> 30 Mar, 2019
                            </p>
                          </div>
                          <p>
                            Aliquam fringilla euismod risus ac bibendum. Sed sit
                            amet sem varius ante feugiat lacinia. Nunc ipsum
                            nulla, vulputate ut venenatis vitae, malesuada ut
                            mi. Quisque iaculis, dui congue placerat pretium,
                            augue erat accumsan lacus
                          </p>
                        </div>
                      </div>
                      <div className="form-group row">
                        <div className="col">
                          <label className="col-form-label">
                            <span className="text-danger">*</span>
                            Your Name
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            required
                          />
                        </div>
                      </div>
                      <div className="form-group row">
                        <div className="col">
                          <label className="col-form-label">
                            <span className="text-danger">*</span>
                            Your Email
                          </label>
                          <input
                            type="email"
                            className="form-control"
                            required
                          />
                        </div>
                      </div>
                      <div className="form-group row">
                        <div className="col">
                          <label className="col-form-label">
                            <span className="text-danger">*</span>
                            Your Review
                          </label>
                          <textarea
                            className="form-control"
                            required
                            defaultValue={""}
                          />
                          <div className="help-block pt-10">
                            <span className="text-danger">Note:</span>
                            HTML is not translated!
                          </div>
                        </div>
                      </div>
                      <div className="form-group row">
                        <div className="col">
                          <label className="col-form-label">
                            <span className="text-danger">*</span>
                            Rating
                          </label>
                          &nbsp;&nbsp;&nbsp; Bad&nbsp;
                          <input type="radio" defaultValue={1} name="rating" />
                          &nbsp;
                          <input type="radio" defaultValue={2} name="rating" />
                          &nbsp;
                          <input type="radio" defaultValue={3} name="rating" />
                          &nbsp;
                          <input type="radio" defaultValue={4} name="rating" />
                          &nbsp;
                          <input
                            type="radio"
                            defaultValue={5}
                            name="rating"
                            defaultChecked
                          />
                          &nbsp;Good
                        </div>
                      </div>
                      <div className="buttons">
                        <button className="btn btn-sqr" type="submit">
                          Continue
                        </button>
                      </div>
                    </form>{" "}
                    {/* end of review-form */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* product details reviews end */}
      </div>
      {/* product details wrapper end */}
    </div>
  </div>
</div>

            </div>
            );
    }
}

export default Product;