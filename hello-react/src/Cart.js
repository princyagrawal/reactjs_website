import React from 'react';
import './App.css';

export class Cart extends React.Component {
    render() {
        return (
            <div classname='Cart'>

<section class="pt-5">
<div className="container">
  <div className="row">
    <div className="col-12 col-lg-12 col-sm-12 col-md-12">
      <div className="minicart-content-box">
        <div className="minicart-item-wrapper">
          <ul>
            <li className="minicart-item">
              <div className="minicart-thumb">
                <a href="product-details.html">
                  <img src="img/cart/cart-1.jpg" alt="product" />
                </a>
              </div>
              <div className="minicart-content">
                <h3 className="product-name">
                  <a href="product-details.html">
                    Dozen White Botanical Linen Dinner Napkins
                  </a>
                </h3>
                <p>
                  <span className="cart-quantity">
                    1 <strong>×</strong>
                  </span>
                  <span className="cart-price">$100.00</span>
                </p>
              </div>
              <button className="minicart-remove">
                <i className="pe-7s-close" />
              </button>
            </li>
            <li className="minicart-item">
              <div className="minicart-thumb">
                <a href="product-details.html">
                  <img src="img/cart/cart-2.jpg" alt="product" />
                </a>
              </div>
              <div className="minicart-content">
                <h3 className="product-name">
                  <a href="product-details.html">
                    Dozen White Botanical Linen Dinner Napkins
                  </a>
                </h3>
                <p>
                  <span className="cart-quantity">
                    1 <strong>×</strong>
                  </span>
                  <span className="cart-price">$80.00</span>
                </p>
              </div>
              <button className="minicart-remove">
                <i className="pe-7s-close" />
              </button>
            </li>
          </ul>
        </div>
        <div className="minicart-pricing-box">
          <ul>
            <li>
              <span>sub-total</span>
              <span>
                <strong>$300.00</strong>
              </span>
            </li>
            <li>
              <span>Eco Tax (-2.00)</span>
              <span>
                <strong>$10.00</strong>
              </span>
            </li>
            <li>
              <span>VAT (20%)</span>
              <span>
                <strong>$60.00</strong>
              </span>
            </li>
            <li className="total">
              <span>total</span>
              <span>
                <strong>$370.00</strong>
              </span>
            </li>
          </ul>
        </div>
        <div className="minicart-button w-25" style={{ marginLeft: "40%" }}>
          <a href="cart.html" className>
            <i className="fa fa-shopping-cart" /> View Cart
          </a>
          <a href="cart.html">
            <i className="fa fa-share" /> Checkout
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

</section>
            </div>
            );
    }
}

export default Cart;