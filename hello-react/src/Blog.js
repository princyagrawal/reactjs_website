import React from 'react';
import './App.css';

export class Blog extends React.Component {
    render() {
        return (
            <div classname='Blog'>

<div className="blog-main-wrapper section-padding">
  <div className="container">
    <div className="row">
      <div className="col-lg-3 order-2 order-lg-1">
        <aside className="blog-sidebar-wrapper">
          <div className="blog-sidebar">
            <h5 className="title">search</h5>
            <div className="sidebar-serch-form">
              <form action="#">
                <input
                  type="text"
                  className="search-field"
                  placeholder="search here"
                />
                <button type="submit" className="search-btn">
                  <i className="fa fa-search" />
                </button>
              </form>
            </div>
          </div>{" "}
          {/* single sidebar end */}
          <div className="blog-sidebar">
            <h5 className="title">categories</h5>
            <ul className="blog-archive blog-category">
              <li>
                <a href="#">Barber (10)</a>
              </li>
              <li>
                <a href="#">fashion (08)</a>
              </li>
              <li>
                <a href="#">handbag (07)</a>
              </li>
              <li>
                <a href="#">Jewelry (14)</a>
              </li>
              <li>
                <a href="#">food (10)</a>
              </li>
            </ul>
          </div>{" "}
          {/* single sidebar end */}
          <div className="blog-sidebar">
            <h5 className="title">Blog Archives</h5>
            <ul className="blog-archive">
              <li>
                <a href="#">January (10)</a>
              </li>
              <li>
                <a href="#">February (08)</a>
              </li>
              <li>
                <a href="#">March (07)</a>
              </li>
              <li>
                <a href="#">April (14)</a>
              </li>
              <li>
                <a href="#">May (10)</a>
              </li>
            </ul>
          </div>{" "}
          {/* single sidebar end */}
          <div className="blog-sidebar">
            <h5 className="title">recent post</h5>
            <div className="recent-post">
              <div className="recent-post-item">
                <figure className="product-thumb">
                  <a href="blog-details.html">
                    <img src="img/blog/blog-img1.jpg" alt="blog image" />
                  </a>
                </figure>
                <div className="recent-post-description">
                  <div className="product-name">
                    <h6>
                      <a href="blog-details.html">Auctor gravida enim</a>
                    </h6>
                    <p>march 10 2019</p>
                  </div>
                </div>
              </div>
              <div className="recent-post-item">
                <figure className="product-thumb">
                  <a href="blog-details.html">
                    <img src="img/blog/blog-img2.jpg" alt="blog image" />
                  </a>
                </figure>
                <div className="recent-post-description">
                  <div className="product-name">
                    <h6>
                      <a href="blog-details.html">gravida auctor dnim</a>
                    </h6>
                    <p>march 18 2019</p>
                  </div>
                </div>
              </div>
              <div className="recent-post-item">
                <figure className="product-thumb">
                  <a href="blog-details.html">
                    <img src="img/blog/blog-img3.jpg" alt="blog image" />
                  </a>
                </figure>
                <div className="recent-post-description">
                  <div className="product-name">
                    <h6>
                      <a href="blog-details.html">enim auctor gravida</a>
                    </h6>
                    <p>march 14 2019</p>
                  </div>
                </div>
              </div>
            </div>
          </div>{" "}
          {/* single sidebar end */}
          <div className="blog-sidebar">
            <h5 className="title">Tags</h5>
            <ul className="blog-tags">
              <li>
                <a href="#">camera</a>
              </li>
              <li>
                <a href="#">computer</a>
              </li>
              <li>
                <a href="#">bag</a>
              </li>
              <li>
                <a href="#">watch</a>
              </li>
              <li>
                <a href="#">smartphone</a>
              </li>
              <li>
                <a href="#">shoes</a>
              </li>
            </ul>
          </div>{" "}
          {/* single sidebar end */}
        </aside>
      </div>
      <div className="col-lg-9 order-1 order-lg-2">
        <div className="blog-item-wrapper">
          {/* blog item wrapper end */}
          <div className="row mbn-30">
            <div className="col-md-6">
              {/* blog post item start */}
              <div className="blog-post-item mb-30">
                <figure className="blog-thumb">
                  <a href="blog-details.html">
                    <img src="img/blog/blog-img1.jpg" alt="blog image" />
                  </a>
                </figure>
                <div className="blog-content">
                  <div className="blog-meta">
                    <p>
                      10/04/2019 | <a href="#">Corano</a>
                    </p>
                  </div>
                  <h4 className="blog-title">
                    <a href="blog-details.html">
                      Celebrity Daughter Opens Up About Having Her Eye Color
                      Changed
                    </a>
                  </h4>
                </div>
              </div>
              {/* blog post item end */}
            </div>
            <div className="col-md-6">
              {/* blog post item start */}
              <div className="blog-post-item mb-30">
              <figure className="blog-thumb">
                  <a href="blog-details.html">
                    <img src="img/blog/blog-img2.jpg" alt="blog image" />
                  </a>
                </figure>
                <div className="blog-content">
                  <div className="blog-meta">
                    <p>
                      12/04/2019 | <a href="#">Corano</a>
                    </p>
                  </div>
                  <h4 className="blog-title">
                    <a href="blog-details.html">
                      Lotto Winner Offering Up Money To Any Man That Will Date
                      Her
                    </a>
                  </h4>
                </div>
              </div>
              {/* blog post item end */}
            </div>
            <div className="col-md-6">
              {/* blog post item start */}
              <div className="blog-post-item mb-30">
              <figure className="blog-thumb">
                  <a href="blog-details.html">
                    <img src="img/blog/blog-img3.jpg" alt="blog image" />
                  </a>
                </figure>
                <div className="blog-content">
                  <div className="blog-meta">
                    <p>
                      15/04/2019 | <a href="#">Corano</a>
                    </p>
                  </div>
                  <h4 className="blog-title">
                    <a href="blog-details.html">
                      Children Left Home Alone For 4 Days In TV series
                      Experiment
                    </a>
                  </h4>
                </div>
              </div>
              {/* blog post item end */}
            </div>
            <div className="col-md-6">
              {/* blog post item start */}
              <div className="blog-post-item mb-30">
              <figure className="blog-thumb">
                  <a href="blog-details.html">
                    <img src="img/blog/blog-img4.jpg" alt="blog image" />
                  </a>
                </figure>
                <div className="blog-content">
                  <div className="blog-meta">
                    <p>
                      05/04/2019 | <a href="#">Corano</a>
                    </p>
                  </div>
                  <h4 className="blog-title">
                    <a href="blog-details.html">
                      People are Willing Lie When Comes Money, According to
                      Research
                    </a>
                  </h4>
                </div>
              </div>
              {/* blog post item end */}
            </div>
            <div className="col-md-6">
              {/* blog post item start */}
              <div className="blog-post-item mb-30">
                <figure className="blog-thumb">
                  <a href="blog-details.html">
                    <img src="img/blog/blog-img5.jpg" alt="blog image" />
                  </a>
                </figure>
                <div className="blog-content">
                  <div className="blog-meta">
                    <p>
                      02/04/2019 | <a href="#">Corano</a>
                    </p>
                  </div>
                  <h4 className="blog-title">
                    <a href="blog-details.html">
                      romantic Love Stories Of Hollywood’s Biggest Celebrities
                    </a>
                  </h4>
                </div>
              </div>
              {/* blog post item end */}
            </div>
            <div className="col-md-6">
              {/* blog post item start */}
              <div className="blog-post-item mb-30">
                <figure className="blog-thumb">
                  <a href="blog-details.html">
                    <img src="img/blog/blog-img3.jpg" alt="blog image" />
                  </a>
                </figure>
                <div className="blog-content">
                  <div className="blog-meta">
                    <p>
                      25/03/2019 | <a href="#">Corano</a>
                    </p>
                  </div>
                  <h4 className="blog-title">
                    <a href="blog-details.html">
                      Celebrity Daughter Opens Up About Having Her Eye Color
                      Changed
                    </a>
                  </h4>
                </div>
              </div>
              {/* blog post item end */}
            </div>
          </div>
          {/* blog item wrapper end */}
          {/* start pagination area */}
          <div className="paginatoin-area text-center">
            <ul className="pagination-box">
              <li>
                <a className="previous" href="#">
                  <i className="pe-7s-angle-left" />
                </a>
              </li>
              <li className="active">
                <a href="#">1</a>
              </li>
              <li>
                <a href="#">2</a>
              </li>
              <li>
                <a href="#">3</a>
              </li>
              <li>
                <a className="next" href="#">
                  <i className="pe-7s-angle-right" />
                </a>
              </li>
            </ul>
          </div>
          {/* end pagination area */}
        </div>
      </div>
    </div>
  </div>
</div>

            </div>
            );
    }
}

export default Blog;