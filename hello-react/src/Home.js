import React from 'react';
import logo from './logo.svg';
import './App.css';

class App extends React.Component {
  render() {
  return (
    <div className="Home">
      
<section className="slider-area">

  <div className="hero-slider-active slick-arrow-style slick-arrow-style_hero slick-dot-style">
    {/* single slider item start */}
    <div className="hero-single-slide hero-overlay">
      <div
        className="hero-slider-item bg-img" 
        style={{backgroundImage: "url('./img/slider/home2-slide2.jpg')"}}
      >
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="hero-slider-content slide-1">
                <h2 className="slide-title">
                  Flower Diamond<span>Collection</span>
                </h2>
                <h4 className="slide-desc">
                  Budget Jewellery Starting At $295.99
                </h4>
                <a href="shop.html" className="btn btn-hero">
                  Read More
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>
<div className="service-policy">
  <div className="container">
    <div className="policy-block section-padding">
      <div className="row mtn-30">
        <div className="col-sm-6 col-lg-3">
          <div className="policy-item">
            <div className="policy-icon">
              <i className="pe-7s-plane" />
            </div>
            <div className="policy-content">
              <h6>Free Shipping</h6>
              <p>Free shipping all order</p>
            </div>
          </div>
        </div>
        <div className="col-sm-6 col-lg-3">
          <div className="policy-item">
            <div className="policy-icon">
              <i className="pe-7s-help2" />
            </div>
            <div className="policy-content">
              <h6>Support 24/7</h6>
              <p>Support 24 hours a day</p>
            </div>
          </div>
        </div>
        <div className="col-sm-6 col-lg-3">
          <div className="policy-item">
            <div className="policy-icon">
              <i className="pe-7s-back" />
            </div>
            <div className="policy-content">
              <h6>Money Return</h6>
              <p>30 days for free return</p>
            </div>
          </div>
        </div>
        <div className="col-sm-6 col-lg-3">
          <div className="policy-item">
            <div className="policy-icon">
              <i className="pe-7s-credit" />
            </div>
            <div className="policy-content">
              <h6>100% Payment Secure</h6>
              <p>We ensure secure payment</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<section className="product-area section-padding">
  <div className="container">
    <div className="row">
      <div className="col-12">
        {/* section title start */}
        <div className="section-title text-center">
          <h2 className="title">our products</h2>
          <p className="sub-title">Add our products to weekly lineup</p>
        </div>
        {/* section title start */}
      </div>
    </div>
    <div className="row">
      <div className="col-12">
        <div className="product-container">
          {/* product tab menu start */}
          <div className="product-tab-menu">
            <ul className="nav justify-content-center">
              <li>
                <a href="#tab1" className="active" data-toggle="tab">
                  Entertainment
                </a>
              </li>
              <li>
                <a href="#tab2" data-toggle="tab">
                  Storage
                </a>
              </li>
              <li>
                <a href="#tab3" data-toggle="tab">
                  Lying
                </a>
              </li>
              <li>
                <a href="#tab4" data-toggle="tab">
                  Tables
                </a>
              </li>
            </ul>
          </div>
          {/* product tab menu end */}
          {/* product tab content start */}
          <div className="tab-content">
            <div className="tab-pane fade show active" id="tab1">
              <div className="product-carousel-4 slick-row-10 slick-arrow-style">
                {/* product item start */}
                <div className="product-item">
                  <figure className="product-thumb">
                    <a href="product-details.html">
                      <img
                        className="pri-img"
                        src="assets/img/product/product-1.jpg"
                        alt="product"
                      />
                      <img
                        className="sec-img"
                        src="assets/img/product/product-18.jpg"
                        alt="product"
                      />
                    </a>
                    <div className="product-badge">
                      <div className="product-label new">
                        <span>new</span>
                      </div>
                      <div className="product-label discount">
                        <span>10%</span>
                      </div>
                    </div>
                    <div className="button-group">
                      <a
                        href="wishlist.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to wishlist"
                      >
                        <i className="pe-7s-like" />
                      </a>
                      <a
                        href="compare.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to Compare"
                      >
                        <i className="pe-7s-refresh-2" />
                      </a>
                      <a href="#" data-toggle="modal" data-target="#quick_view">
                        <span
                          data-toggle="tooltip"
                          data-placement="left"
                          title="Quick View"
                        >
                          <i className="pe-7s-search" />
                        </span>
                      </a>
                    </div>
                    <div className="cart-hover">
                      <button className="btn btn-cart">add to cart</button>
                    </div>
                  </figure>
                  <div className="product-caption text-center">
                    <div className="product-identity">
                      <p className="manufacturer-name">
                        <a href="product-details.html">Gold</a>
                      </p>
                    </div>
                    <ul className="color-categories">
                      <li>
                        <a
                          className="c-lightblue"
                          href="#"
                          title="LightSteelblue"
                        />
                      </li>
                      <li>
                        <a className="c-darktan" href="#" title="Darktan" />
                      </li>
                      <li>
                        <a className="c-grey" href="#" title="Grey" />
                      </li>
                      <li>
                        <a className="c-brown" href="#" title="Brown" />
                      </li>
                    </ul>
                    <h6 className="product-name">
                      <a href="product-details.html">Perfect Diamond Jewelry</a>
                    </h6>
                    <div className="price-box">
                      <span className="price-regular">$60.00</span>
                      <span className="price-old">
                        <del>$70.00</del>
                      </span>
                    </div>
                  </div>
                </div>
                {/* product item end */}
                {/* product item start */}
                <div className="product-item">
                  <figure className="product-thumb">
                    <a href="product-details.html">
                      <img
                        className="pri-img"
                        src="assets/img/product/product-2.jpg"
                        alt="product"
                      />
                      <img
                        className="sec-img"
                        src="assets/img/product/product-17.jpg"
                        alt="product"
                      />
                    </a>
                    <div className="product-badge">
                      <div className="product-label new">
                        <span>sale</span>
                      </div>
                      <div className="product-label discount">
                        <span>new</span>
                      </div>
                    </div>
                    <div className="button-group">
                      <a
                        href="wishlist.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to wishlist"
                      >
                        <i className="pe-7s-like" />
                      </a>
                      <a
                        href="compare.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to Compare"
                      >
                        <i className="pe-7s-refresh-2" />
                      </a>
                      <a href="#" data-toggle="modal" data-target="#quick_view">
                        <span
                          data-toggle="tooltip"
                          data-placement="left"
                          title="Quick View"
                        >
                          <i className="pe-7s-search" />
                        </span>
                      </a>
                    </div>
                    <div className="cart-hover">
                      <button className="btn btn-cart">add to cart</button>
                    </div>
                  </figure>
                  <div className="product-caption text-center">
                    <div className="product-identity">
                      <p className="manufacturer-name">
                        <a href="product-details.html">mony</a>
                      </p>
                    </div>
                    <ul className="color-categories">
                      <li>
                        <a
                          className="c-lightblue"
                          href="#"
                          title="LightSteelblue"
                        />
                      </li>
                      <li>
                        <a className="c-darktan" href="#" title="Darktan" />
                      </li>
                      <li>
                        <a className="c-grey" href="#" title="Grey" />
                      </li>
                      <li>
                        <a className="c-brown" href="#" title="Brown" />
                      </li>
                    </ul>
                    <h6 className="product-name">
                      <a href="product-details.html">
                        Handmade Golden Necklace
                      </a>
                    </h6>
                    <div className="price-box">
                      <span className="price-regular">$50.00</span>
                      <span className="price-old">
                        <del>$80.00</del>
                      </span>
                    </div>
                  </div>
                </div>
                {/* product item end */}
                {/* product item start */}
                <div className="product-item">
                  <figure className="product-thumb">
                    <a href="product-details.html">
                      <img
                        className="pri-img"
                        src="assets/img/product/product-3.jpg"
                        alt="product"
                      />
                      <img
                        className="sec-img"
                        src="assets/img/product/product-16.jpg"
                        alt="product"
                      />
                    </a>
                    <div className="product-badge">
                      <div className="product-label new">
                        <span>new</span>
                      </div>
                    </div>
                    <div className="button-group">
                      <a
                        href="wishlist.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to wishlist"
                      >
                        <i className="pe-7s-like" />
                      </a>
                      <a
                        href="compare.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to Compare"
                      >
                        <i className="pe-7s-refresh-2" />
                      </a>
                      <a href="#" data-toggle="modal" data-target="#quick_view">
                        <span
                          data-toggle="tooltip"
                          data-placement="left"
                          title="Quick View"
                        >
                          <i className="pe-7s-search" />
                        </span>
                      </a>
                    </div>
                    <div className="cart-hover">
                      <button className="btn btn-cart">add to cart</button>
                    </div>
                  </figure>
                  <div className="product-caption text-center">
                    <div className="product-identity">
                      <p className="manufacturer-name">
                        <a href="product-details.html">Diamond</a>
                      </p>
                    </div>
                    <ul className="color-categories">
                      <li>
                        <a
                          className="c-lightblue"
                          href="#"
                          title="LightSteelblue"
                        />
                      </li>
                      <li>
                        <a className="c-darktan" href="#" title="Darktan" />
                      </li>
                      <li>
                        <a className="c-grey" href="#" title="Grey" />
                      </li>
                      <li>
                        <a className="c-brown" href="#" title="Brown" />
                      </li>
                    </ul>
                    <h6 className="product-name">
                      <a href="product-details.html">Perfect Diamond Jewelry</a>
                    </h6>
                    <div className="price-box">
                      <span className="price-regular">$99.00</span>
                      <span className="price-old">
                        <del />
                      </span>
                    </div>
                  </div>
                </div>
                {/* product item end */}
                {/* product item start */}
                <div className="product-item">
                  <figure className="product-thumb">
                    <a href="product-details.html">
                      <img
                        className="pri-img"
                        src="assets/img/product/product-4.jpg"
                        alt="product"
                      />
                      <img
                        className="sec-img"
                        src="assets/img/product/product-15.jpg"
                        alt="product"
                      />
                    </a>
                    <div className="product-badge">
                      <div className="product-label new">
                        <span>sale</span>
                      </div>
                      <div className="product-label discount">
                        <span>15%</span>
                      </div>
                    </div>
                    <div className="button-group">
                      <a
                        href="wishlist.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to wishlist"
                      >
                        <i className="pe-7s-like" />
                      </a>
                      <a
                        href="compare.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to Compare"
                      >
                        <i className="pe-7s-refresh-2" />
                      </a>
                      <a href="#" data-toggle="modal" data-target="#quick_view">
                        <span
                          data-toggle="tooltip"
                          data-placement="left"
                          title="Quick View"
                        >
                          <i className="pe-7s-search" />
                        </span>
                      </a>
                    </div>
                    <div className="cart-hover">
                      <button className="btn btn-cart">add to cart</button>
                    </div>
                  </figure>
                  <div className="product-caption text-center">
                    <div className="product-identity">
                      <p className="manufacturer-name">
                        <a href="product-details.html">silver</a>
                      </p>
                    </div>
                    <ul className="color-categories">
                      <li>
                        <a
                          className="c-lightblue"
                          href="#"
                          title="LightSteelblue"
                        />
                      </li>
                      <li>
                        <a className="c-darktan" href="#" title="Darktan" />
                      </li>
                      <li>
                        <a className="c-grey" href="#" title="Grey" />
                      </li>
                      <li>
                        <a className="c-brown" href="#" title="Brown" />
                      </li>
                    </ul>
                    <h6 className="product-name">
                      <a href="product-details.html">
                        Diamond Exclusive Ornament
                      </a>
                    </h6>
                    <div className="price-box">
                      <span className="price-regular">$55.00</span>
                      <span className="price-old">
                        <del>$75.00</del>
                      </span>
                    </div>
                  </div>
                </div>
                {/* product item end */}
                {/* product item start */}
                <div className="product-item">
                  <figure className="product-thumb">
                    <a href="product-details.html">
                      <img
                        className="pri-img"
                        src="assets/img/product/product-5.jpg"
                        alt="product"
                      />
                      <img
                        className="sec-img"
                        src="assets/img/product/product-14.jpg"
                        alt="product"
                      />
                    </a>
                    <div className="product-badge">
                      <div className="product-label new">
                        <span>new</span>
                      </div>
                      <div className="product-label discount">
                        <span>20%</span>
                      </div>
                    </div>
                    <div className="button-group">
                      <a
                        href="wishlist.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to wishlist"
                      >
                        <i className="pe-7s-like" />
                      </a>
                      <a
                        href="compare.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to Compare"
                      >
                        <i className="pe-7s-refresh-2" />
                      </a>
                      <a href="#" data-toggle="modal" data-target="#quick_view">
                        <span
                          data-toggle="tooltip"
                          data-placement="left"
                          title="Quick View"
                        >
                          <i className="pe-7s-search" />
                        </span>
                      </a>
                    </div>
                    <div className="cart-hover">
                      <button className="btn btn-cart">add to cart</button>
                    </div>
                  </figure>
                  <div className="product-caption text-center">
                    <div className="product-identity">
                      <p className="manufacturer-name">
                        <a href="product-details.html">mony</a>
                      </p>
                    </div>
                    <ul className="color-categories">
                      <li>
                        <a
                          className="c-lightblue"
                          href="#"
                          title="LightSteelblue"
                        />
                      </li>
                      <li>
                        <a className="c-darktan" href="#" title="Darktan" />
                      </li>
                      <li>
                        <a className="c-grey" href="#" title="Grey" />
                      </li>
                      <li>
                        <a className="c-brown" href="#" title="Brown" />
                      </li>
                    </ul>
                    <h6 className="product-name">
                      <a href="product-details.html">Citygold Exclusive Ring</a>
                    </h6>
                    <div className="price-box">
                      <span className="price-regular">$60.00</span>
                      <span className="price-old">
                        <del>$70.00</del>
                      </span>
                    </div>
                  </div>
                </div>
                {/* product item end */}
              </div>
            </div>
            <div className="tab-pane fade" id="tab2">
              <div className="product-carousel-4 slick-row-10 slick-arrow-style">
                {/* product item start */}
                <div className="product-item">
                  <figure className="product-thumb">
                    <a href="product-details.html">
                      <img
                        className="pri-img"
                        src="assets/img/product/product-6.jpg"
                        alt="product"
                      />
                      <img
                        className="sec-img"
                        src="assets/img/product/product-13.jpg"
                        alt="product"
                      />
                    </a>
                    <div className="product-badge">
                      <div className="product-label new">
                        <span>new</span>
                      </div>
                      <div className="product-label discount">
                        <span>10%</span>
                      </div>
                    </div>
                    <div className="button-group">
                      <a
                        href="wishlist.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to wishlist"
                      >
                        <i className="pe-7s-like" />
                      </a>
                      <a
                        href="compare.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to Compare"
                      >
                        <i className="pe-7s-refresh-2" />
                      </a>
                      <a href="#" data-toggle="modal" data-target="#quick_view">
                        <span
                          data-toggle="tooltip"
                          data-placement="left"
                          title="Quick View"
                        >
                          <i className="pe-7s-search" />
                        </span>
                      </a>
                    </div>
                    <div className="cart-hover">
                      <button className="btn btn-cart">add to cart</button>
                    </div>
                  </figure>
                  <div className="product-caption text-center">
                    <div className="product-identity">
                      <p className="manufacturer-name">
                        <a href="product-details.html">Gold</a>
                      </p>
                    </div>
                    <ul className="color-categories">
                      <li>
                        <a
                          className="c-lightblue"
                          href="#"
                          title="LightSteelblue"
                        />
                      </li>
                      <li>
                        <a className="c-darktan" href="#" title="Darktan" />
                      </li>
                      <li>
                        <a className="c-grey" href="#" title="Grey" />
                      </li>
                      <li>
                        <a className="c-brown" href="#" title="Brown" />
                      </li>
                    </ul>
                    <h6 className="product-name">
                      <a href="product-details.html">Perfect Diamond Jewelry</a>
                    </h6>
                    <div className="price-box">
                      <span className="price-regular">$60.00</span>
                      <span className="price-old">
                        <del>$70.00</del>
                      </span>
                    </div>
                  </div>
                </div>
                {/* product item end */}
                {/* product item start */}
                <div className="product-item">
                  <figure className="product-thumb">
                    <a href="product-details.html">
                      <img
                        className="pri-img"
                        src="assets/img/product/product-7.jpg"
                        alt="product"
                      />
                      <img
                        className="sec-img"
                        src="assets/img/product/product-12.jpg"
                        alt="product"
                      />
                    </a>
                    <div className="product-badge">
                      <div className="product-label new">
                        <span>sale</span>
                      </div>
                      <div className="product-label discount">
                        <span>new</span>
                      </div>
                    </div>
                    <div className="button-group">
                      <a
                        href="wishlist.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to wishlist"
                      >
                        <i className="pe-7s-like" />
                      </a>
                      <a
                        href="compare.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to Compare"
                      >
                        <i className="pe-7s-refresh-2" />
                      </a>
                      <a href="#" data-toggle="modal" data-target="#quick_view">
                        <span
                          data-toggle="tooltip"
                          data-placement="left"
                          title="Quick View"
                        >
                          <i className="pe-7s-search" />
                        </span>
                      </a>
                    </div>
                    <div className="cart-hover">
                      <button className="btn btn-cart">add to cart</button>
                    </div>
                  </figure>
                  <div className="product-caption text-center">
                    <div className="product-identity">
                      <p className="manufacturer-name">
                        <a href="product-details.html">mony</a>
                      </p>
                    </div>
                    <ul className="color-categories">
                      <li>
                        <a
                          className="c-lightblue"
                          href="#"
                          title="LightSteelblue"
                        />
                      </li>
                      <li>
                        <a className="c-darktan" href="#" title="Darktan" />
                      </li>
                      <li>
                        <a className="c-grey" href="#" title="Grey" />
                      </li>
                      <li>
                        <a className="c-brown" href="#" title="Brown" />
                      </li>
                    </ul>
                    <h6 className="product-name">
                      <a href="product-details.html">
                        Handmade Golden Necklace
                      </a>
                    </h6>
                    <div className="price-box">
                      <span className="price-regular">$50.00</span>
                      <span className="price-old">
                        <del>$80.00</del>
                      </span>
                    </div>
                  </div>
                </div>
                {/* product item end */}
                {/* product item start */}
                <div className="product-item">
                  <figure className="product-thumb">
                    <a href="product-details.html">
                      <img
                        className="pri-img"
                        src="assets/img/product/product-8.jpg"
                        alt="product"
                      />
                      <img
                        className="sec-img"
                        src="assets/img/product/product-11.jpg"
                        alt="product"
                      />
                    </a>
                    <div className="product-badge">
                      <div className="product-label new">
                        <span>new</span>
                      </div>
                    </div>
                    <div className="button-group">
                      <a
                        href="wishlist.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to wishlist"
                      >
                        <i className="pe-7s-like" />
                      </a>
                      <a
                        href="compare.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to Compare"
                      >
                        <i className="pe-7s-refresh-2" />
                      </a>
                      <a href="#" data-toggle="modal" data-target="#quick_view">
                        <span
                          data-toggle="tooltip"
                          data-placement="left"
                          title="Quick View"
                        >
                          <i className="pe-7s-search" />
                        </span>
                      </a>
                    </div>
                    <div className="cart-hover">
                      <button className="btn btn-cart">add to cart</button>
                    </div>
                  </figure>
                  <div className="product-caption text-center">
                    <div className="product-identity">
                      <p className="manufacturer-name">
                        <a href="product-details.html">Diamond</a>
                      </p>
                    </div>
                    <ul className="color-categories">
                      <li>
                        <a
                          className="c-lightblue"
                          href="#"
                          title="LightSteelblue"
                        />
                      </li>
                      <li>
                        <a className="c-darktan" href="#" title="Darktan" />
                      </li>
                      <li>
                        <a className="c-grey" href="#" title="Grey" />
                      </li>
                      <li>
                        <a className="c-brown" href="#" title="Brown" />
                      </li>
                    </ul>
                    <h6 className="product-name">
                      <a href="product-details.html">Perfect Diamond Jewelry</a>
                    </h6>
                    <div className="price-box">
                      <span className="price-regular">$99.00</span>
                      <span className="price-old">
                        <del />
                      </span>
                    </div>
                  </div>
                </div>
                {/* product item end */}
                {/* product item start */}
                <div className="product-item">
                  <figure className="product-thumb">
                    <a href="product-details.html">
                      <img
                        className="pri-img"
                        src="assets/img/product/product-9.jpg"
                        alt="product"
                      />
                      <img
                        className="sec-img"
                        src="assets/img/product/product-10.jpg"
                        alt="product"
                      />
                    </a>
                    <div className="product-badge">
                      <div className="product-label new">
                        <span>sale</span>
                      </div>
                      <div className="product-label discount">
                        <span>15%</span>
                      </div>
                    </div>
                    <div className="button-group">
                      <a
                        href="wishlist.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to wishlist"
                      >
                        <i className="pe-7s-like" />
                      </a>
                      <a
                        href="compare.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to Compare"
                      >
                        <i className="pe-7s-refresh-2" />
                      </a>
                      <a href="#" data-toggle="modal" data-target="#quick_view">
                        <span
                          data-toggle="tooltip"
                          data-placement="left"
                          title="Quick View"
                        >
                          <i className="pe-7s-search" />
                        </span>
                      </a>
                    </div>
                    <div className="cart-hover">
                      <button className="btn btn-cart">add to cart</button>
                    </div>
                  </figure>
                  <div className="product-caption text-center">
                    <div className="product-identity">
                      <p className="manufacturer-name">
                        <a href="product-details.html">silver</a>
                      </p>
                    </div>
                    <ul className="color-categories">
                      <li>
                        <a
                          className="c-lightblue"
                          href="#"
                          title="LightSteelblue"
                        />
                      </li>
                      <li>
                        <a className="c-darktan" href="#" title="Darktan" />
                      </li>
                      <li>
                        <a className="c-grey" href="#" title="Grey" />
                      </li>
                      <li>
                        <a className="c-brown" href="#" title="Brown" />
                      </li>
                    </ul>
                    <h6 className="product-name">
                      <a href="product-details.html">
                        Diamond Exclusive Ornament
                      </a>
                    </h6>
                    <div className="price-box">
                      <span className="price-regular">$55.00</span>
                      <span className="price-old">
                        <del>$75.00</del>
                      </span>
                    </div>
                  </div>
                </div>
                {/* product item end */}
                {/* product item start */}
                <div className="product-item">
                  <figure className="product-thumb">
                    <a href="product-details.html">
                      <img
                        className="pri-img"
                        src="assets/img/product/product-10.jpg"
                        alt="product"
                      />
                      <img
                        className="sec-img"
                        src="assets/img/product/product-9.jpg"
                        alt="product"
                      />
                    </a>
                    <div className="product-badge">
                      <div className="product-label new">
                        <span>new</span>
                      </div>
                      <div className="product-label discount">
                        <span>20%</span>
                      </div>
                    </div>
                    <div className="button-group">
                      <a
                        href="wishlist.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to wishlist"
                      >
                        <i className="pe-7s-like" />
                      </a>
                      <a
                        href="compare.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to Compare"
                      >
                        <i className="pe-7s-refresh-2" />
                      </a>
                      <a href="#" data-toggle="modal" data-target="#quick_view">
                        <span
                          data-toggle="tooltip"
                          data-placement="left"
                          title="Quick View"
                        >
                          <i className="pe-7s-search" />
                        </span>
                      </a>
                    </div>
                    <div className="cart-hover">
                      <button className="btn btn-cart">add to cart</button>
                    </div>
                  </figure>
                  <div className="product-caption text-center">
                    <div className="product-identity">
                      <p className="manufacturer-name">
                        <a href="product-details.html">mony</a>
                      </p>
                    </div>
                    <ul className="color-categories">
                      <li>
                        <a
                          className="c-lightblue"
                          href="#"
                          title="LightSteelblue"
                        />
                      </li>
                      <li>
                        <a className="c-darktan" href="#" title="Darktan" />
                      </li>
                      <li>
                        <a className="c-grey" href="#" title="Grey" />
                      </li>
                      <li>
                        <a className="c-brown" href="#" title="Brown" />
                      </li>
                    </ul>
                    <h6 className="product-name">
                      <a href="product-details.html">Citygold Exclusive Ring</a>
                    </h6>
                    <div className="price-box">
                      <span className="price-regular">$60.00</span>
                      <span className="price-old">
                        <del>$70.00</del>
                      </span>
                    </div>
                  </div>
                </div>
                {/* product item end */}
              </div>
            </div>
            <div className="tab-pane fade" id="tab3">
              <div className="product-carousel-4 slick-row-10 slick-arrow-style">
                {/* product item start */}
                <div className="product-item">
                  <figure className="product-thumb">
                    <a href="product-details.html">
                      <img
                        className="pri-img"
                        src="assets/img/product/product-11.jpg"
                        alt="product"
                      />
                      <img
                        className="sec-img"
                        src="assets/img/product/product-8.jpg"
                        alt="product"
                      />
                    </a>
                    <div className="product-badge">
                      <div className="product-label new">
                        <span>new</span>
                      </div>
                      <div className="product-label discount">
                        <span>10%</span>
                      </div>
                    </div>
                    <div className="button-group">
                      <a
                        href="wishlist.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to wishlist"
                      >
                        <i className="pe-7s-like" />
                      </a>
                      <a
                        href="compare.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to Compare"
                      >
                        <i className="pe-7s-refresh-2" />
                      </a>
                      <a href="#" data-toggle="modal" data-target="#quick_view">
                        <span
                          data-toggle="tooltip"
                          data-placement="left"
                          title="Quick View"
                        >
                          <i className="pe-7s-search" />
                        </span>
                      </a>
                    </div>
                    <div className="cart-hover">
                      <button className="btn btn-cart">add to cart</button>
                    </div>
                  </figure>
                  <div className="product-caption text-center">
                    <div className="product-identity">
                      <p className="manufacturer-name">
                        <a href="product-details.html">Gold</a>
                      </p>
                    </div>
                    <ul className="color-categories">
                      <li>
                        <a
                          className="c-lightblue"
                          href="#"
                          title="LightSteelblue"
                        />
                      </li>
                      <li>
                        <a className="c-darktan" href="#" title="Darktan" />
                      </li>
                      <li>
                        <a className="c-grey" href="#" title="Grey" />
                      </li>
                      <li>
                        <a className="c-brown" href="#" title="Brown" />
                      </li>
                    </ul>
                    <h6 className="product-name">
                      <a href="product-details.html">Perfect Diamond Jewelry</a>
                    </h6>
                    <div className="price-box">
                      <span className="price-regular">$60.00</span>
                      <span className="price-old">
                        <del>$70.00</del>
                      </span>
                    </div>
                  </div>
                </div>
                {/* product item end */}
                {/* product item start */}
                <div className="product-item">
                  <figure className="product-thumb">
                    <a href="product-details.html">
                      <img
                        className="pri-img"
                        src="assets/img/product/product-12.jpg"
                        alt="product"
                      />
                      <img
                        className="sec-img"
                        src="assets/img/product/product-7.jpg"
                        alt="product"
                      />
                    </a>
                    <div className="product-badge">
                      <div className="product-label new">
                        <span>sale</span>
                      </div>
                      <div className="product-label discount">
                        <span>new</span>
                      </div>
                    </div>
                    <div className="button-group">
                      <a
                        href="wishlist.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to wishlist"
                      >
                        <i className="pe-7s-like" />
                      </a>
                      <a
                        href="compare.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to Compare"
                      >
                        <i className="pe-7s-refresh-2" />
                      </a>
                      <a href="#" data-toggle="modal" data-target="#quick_view">
                        <span
                          data-toggle="tooltip"
                          data-placement="left"
                          title="Quick View"
                        >
                          <i className="pe-7s-search" />
                        </span>
                      </a>
                    </div>
                    <div className="cart-hover">
                      <button className="btn btn-cart">add to cart</button>
                    </div>
                  </figure>
                  <div className="product-caption text-center">
                    <div className="product-identity">
                      <p className="manufacturer-name">
                        <a href="product-details.html">mony</a>
                      </p>
                    </div>
                    <ul className="color-categories">
                      <li>
                        <a
                          className="c-lightblue"
                          href="#"
                          title="LightSteelblue"
                        />
                      </li>
                      <li>
                        <a className="c-darktan" href="#" title="Darktan" />
                      </li>
                      <li>
                        <a className="c-grey" href="#" title="Grey" />
                      </li>
                      <li>
                        <a className="c-brown" href="#" title="Brown" />
                      </li>
                    </ul>
                    <h6 className="product-name">
                      <a href="product-details.html">
                        Handmade Golden Necklace
                      </a>
                    </h6>
                    <div className="price-box">
                      <span className="price-regular">$50.00</span>
                      <span className="price-old">
                        <del>$80.00</del>
                      </span>
                    </div>
                  </div>
                </div>
                {/* product item end */}
                {/* product item start */}
                <div className="product-item">
                  <figure className="product-thumb">
                    <a href="product-details.html">
                      <img
                        className="pri-img"
                        src="assets/img/product/product-13.jpg"
                        alt="product"
                      />
                      <img
                        className="sec-img"
                        src="assets/img/product/product-6.jpg"
                        alt="product"
                      />
                    </a>
                    <div className="product-badge">
                      <div className="product-label new">
                        <span>new</span>
                      </div>
                    </div>
                    <div className="button-group">
                      <a
                        href="wishlist.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to wishlist"
                      >
                        <i className="pe-7s-like" />
                      </a>
                      <a
                        href="compare.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to Compare"
                      >
                        <i className="pe-7s-refresh-2" />
                      </a>
                      <a href="#" data-toggle="modal" data-target="#quick_view">
                        <span
                          data-toggle="tooltip"
                          data-placement="left"
                          title="Quick View"
                        >
                          <i className="pe-7s-search" />
                        </span>
                      </a>
                    </div>
                    <div className="cart-hover">
                      <button className="btn btn-cart">add to cart</button>
                    </div>
                  </figure>
                  <div className="product-caption text-center">
                    <div className="product-identity">
                      <p className="manufacturer-name">
                        <a href="product-details.html">Diamond</a>
                      </p>
                    </div>
                    <ul className="color-categories">
                      <li>
                        <a
                          className="c-lightblue"
                          href="#"
                          title="LightSteelblue"
                        />
                      </li>
                      <li>
                        <a className="c-darktan" href="#" title="Darktan" />
                      </li>
                      <li>
                        <a className="c-grey" href="#" title="Grey" />
                      </li>
                      <li>
                        <a className="c-brown" href="#" title="Brown" />
                      </li>
                    </ul>
                    <h6 className="product-name">
                      <a href="product-details.html">Perfect Diamond Jewelry</a>
                    </h6>
                    <div className="price-box">
                      <span className="price-regular">$99.00</span>
                      <span className="price-old">
                        <del />
                      </span>
                    </div>
                  </div>
                </div>
                {/* product item end */}
                {/* product item start */}
                <div className="product-item">
                  <figure className="product-thumb">
                    <a href="product-details.html">
                      <img
                        className="pri-img"
                        src="assets/img/product/product-14.jpg"
                        alt="product"
                      />
                      <img
                        className="sec-img"
                        src="assets/img/product/product-5.jpg"
                        alt="product"
                      />
                    </a>
                    <div className="product-badge">
                      <div className="product-label new">
                        <span>sale</span>
                      </div>
                      <div className="product-label discount">
                        <span>15%</span>
                      </div>
                    </div>
                    <div className="button-group">
                      <a
                        href="wishlist.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to wishlist"
                      >
                        <i className="pe-7s-like" />
                      </a>
                      <a
                        href="compare.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to Compare"
                      >
                        <i className="pe-7s-refresh-2" />
                      </a>
                      <a href="#" data-toggle="modal" data-target="#quick_view">
                        <span
                          data-toggle="tooltip"
                          data-placement="left"
                          title="Quick View"
                        >
                          <i className="pe-7s-search" />
                        </span>
                      </a>
                    </div>
                    <div className="cart-hover">
                      <button className="btn btn-cart">add to cart</button>
                    </div>
                  </figure>
                  <div className="product-caption text-center">
                    <div className="product-identity">
                      <p className="manufacturer-name">
                        <a href="product-details.html">silver</a>
                      </p>
                    </div>
                    <ul className="color-categories">
                      <li>
                        <a
                          className="c-lightblue"
                          href="#"
                          title="LightSteelblue"
                        />
                      </li>
                      <li>
                        <a className="c-darktan" href="#" title="Darktan" />
                      </li>
                      <li>
                        <a className="c-grey" href="#" title="Grey" />
                      </li>
                      <li>
                        <a className="c-brown" href="#" title="Brown" />
                      </li>
                    </ul>
                    <h6 className="product-name">
                      <a href="product-details.html">
                        Diamond Exclusive Ornament
                      </a>
                    </h6>
                    <div className="price-box">
                      <span className="price-regular">$55.00</span>
                      <span className="price-old">
                        <del>$75.00</del>
                      </span>
                    </div>
                  </div>
                </div>
                {/* product item end */}
                {/* product item start */}
                <div className="product-item">
                  <figure className="product-thumb">
                    <a href="product-details.html">
                      <img
                        className="pri-img"
                        src="assets/img/product/product-15.jpg"
                        alt="product"
                      />
                      <img
                        className="sec-img"
                        src="assets/img/product/product-4.jpg"
                        alt="product"
                      />
                    </a>
                    <div className="product-badge">
                      <div className="product-label new">
                        <span>new</span>
                      </div>
                      <div className="product-label discount">
                        <span>20%</span>
                      </div>
                    </div>
                    <div className="button-group">
                      <a
                        href="wishlist.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to wishlist"
                      >
                        <i className="pe-7s-like" />
                      </a>
                      <a
                        href="compare.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to Compare"
                      >
                        <i className="pe-7s-refresh-2" />
                      </a>
                      <a href="#" data-toggle="modal" data-target="#quick_view">
                        <span
                          data-toggle="tooltip"
                          data-placement="left"
                          title="Quick View"
                        >
                          <i className="pe-7s-search" />
                        </span>
                      </a>
                    </div>
                    <div className="cart-hover">
                      <button className="btn btn-cart">add to cart</button>
                    </div>
                  </figure>
                  <div className="product-caption text-center">
                    <div className="product-identity">
                      <p className="manufacturer-name">
                        <a href="product-details.html">mony</a>
                      </p>
                    </div>
                    <ul className="color-categories">
                      <li>
                        <a
                          className="c-lightblue"
                          href="#"
                          title="LightSteelblue"
                        />
                      </li>
                      <li>
                        <a className="c-darktan" href="#" title="Darktan" />
                      </li>
                      <li>
                        <a className="c-grey" href="#" title="Grey" />
                      </li>
                      <li>
                        <a className="c-brown" href="#" title="Brown" />
                      </li>
                    </ul>
                    <h6 className="product-name">
                      <a href="product-details.html">Citygold Exclusive Ring</a>
                    </h6>
                    <div className="price-box">
                      <span className="price-regular">$60.00</span>
                      <span className="price-old">
                        <del>$70.00</del>
                      </span>
                    </div>
                  </div>
                </div>
                {/* product item end */}
              </div>
            </div>
            <div className="tab-pane fade" id="tab4">
              <div className="product-carousel-4 slick-row-10 slick-arrow-style">
                {/* product item start */}
                <div className="product-item">
                  <figure className="product-thumb">
                    <a href="product-details.html">
                      <img
                        className="pri-img"
                        src="assets/img/product/product-16.jpg"
                        alt="product"
                      />
                      <img
                        className="sec-img"
                        src="assets/img/product/product-3.jpg"
                        alt="product"
                      />
                    </a>
                    <div className="product-badge">
                      <div className="product-label new">
                        <span>new</span>
                      </div>
                      <div className="product-label discount">
                        <span>10%</span>
                      </div>
                    </div>
                    <div className="button-group">
                      <a
                        href="wishlist.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to wishlist"
                      >
                        <i className="pe-7s-like" />
                      </a>
                      <a
                        href="compare.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to Compare"
                      >
                        <i className="pe-7s-refresh-2" />
                      </a>
                      <a href="#" data-toggle="modal" data-target="#quick_view">
                        <span
                          data-toggle="tooltip"
                          data-placement="left"
                          title="Quick View"
                        >
                          <i className="pe-7s-search" />
                        </span>
                      </a>
                    </div>
                    <div className="cart-hover">
                      <button className="btn btn-cart">add to cart</button>
                    </div>
                  </figure>
                  <div className="product-caption text-center">
                    <div className="product-identity">
                      <p className="manufacturer-name">
                        <a href="product-details.html">Gold</a>
                      </p>
                    </div>
                    <ul className="color-categories">
                      <li>
                        <a
                          className="c-lightblue"
                          href="#"
                          title="LightSteelblue"
                        />
                      </li>
                      <li>
                        <a className="c-darktan" href="#" title="Darktan" />
                      </li>
                      <li>
                        <a className="c-grey" href="#" title="Grey" />
                      </li>
                      <li>
                        <a className="c-brown" href="#" title="Brown" />
                      </li>
                    </ul>
                    <h6 className="product-name">
                      <a href="product-details.html">Perfect Diamond Jewelry</a>
                    </h6>
                    <div className="price-box">
                      <span className="price-regular">$60.00</span>
                      <span className="price-old">
                        <del>$70.00</del>
                      </span>
                    </div>
                  </div>
                </div>
                {/* product item end */}
                {/* product item start */}
                <div className="product-item">
                  <figure className="product-thumb">
                    <a href="product-details.html">
                      <img
                        className="pri-img"
                        src="assets/img/product/product-17.jpg"
                        alt="product"
                      />
                      <img
                        className="sec-img"
                        src="assets/img/product/product-2.jpg"
                        alt="product"
                      />
                    </a>
                    <div className="product-badge">
                      <div className="product-label new">
                        <span>sale</span>
                      </div>
                      <div className="product-label discount">
                        <span>new</span>
                      </div>
                    </div>
                    <div className="button-group">
                      <a
                        href="wishlist.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to wishlist"
                      >
                        <i className="pe-7s-like" />
                      </a>
                      <a
                        href="compare.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to Compare"
                      >
                        <i className="pe-7s-refresh-2" />
                      </a>
                      <a href="#" data-toggle="modal" data-target="#quick_view">
                        <span
                          data-toggle="tooltip"
                          data-placement="left"
                          title="Quick View"
                        >
                          <i className="pe-7s-search" />
                        </span>
                      </a>
                    </div>
                    <div className="cart-hover">
                      <button className="btn btn-cart">add to cart</button>
                    </div>
                  </figure>
                  <div className="product-caption text-center">
                    <div className="product-identity">
                      <p className="manufacturer-name">
                        <a href="product-details.html">mony</a>
                      </p>
                    </div>
                    <ul className="color-categories">
                      <li>
                        <a
                          className="c-lightblue"
                          href="#"
                          title="LightSteelblue"
                        />
                      </li>
                      <li>
                        <a className="c-darktan" href="#" title="Darktan" />
                      </li>
                      <li>
                        <a className="c-grey" href="#" title="Grey" />
                      </li>
                      <li>
                        <a className="c-brown" href="#" title="Brown" />
                      </li>
                    </ul>
                    <h6 className="product-name">
                      <a href="product-details.html">
                        Handmade Golden Necklace
                      </a>
                    </h6>
                    <div className="price-box">
                      <span className="price-regular">$50.00</span>
                      <span className="price-old">
                        <del>$80.00</del>
                      </span>
                    </div>
                  </div>
                </div>
                {/* product item end */}
                {/* product item start */}
                <div className="product-item">
                  <figure className="product-thumb">
                    <a href="product-details.html">
                      <img
                        className="pri-img"
                        src="assets/img/product/product-18.jpg"
                        alt="product"
                      />
                      <img
                        className="sec-img"
                        src="assets/img/product/product-1.jpg"
                        alt="product"
                      />
                    </a>
                    <div className="product-badge">
                      <div className="product-label new">
                        <span>new</span>
                      </div>
                    </div>
                    <div className="button-group">
                      <a
                        href="wishlist.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to wishlist"
                      >
                        <i className="pe-7s-like" />
                      </a>
                      <a
                        href="compare.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to Compare"
                      >
                        <i className="pe-7s-refresh-2" />
                      </a>
                      <a href="#" data-toggle="modal" data-target="#quick_view">
                        <span
                          data-toggle="tooltip"
                          data-placement="left"
                          title="Quick View"
                        >
                          <i className="pe-7s-search" />
                        </span>
                      </a>
                    </div>
                    <div className="cart-hover">
                      <button className="btn btn-cart">add to cart</button>
                    </div>
                  </figure>
                  <div className="product-caption text-center">
                    <div className="product-identity">
                      <p className="manufacturer-name">
                        <a href="product-details.html">Diamond</a>
                      </p>
                    </div>
                    <ul className="color-categories">
                      <li>
                        <a
                          className="c-lightblue"
                          href="#"
                          title="LightSteelblue"
                        />
                      </li>
                      <li>
                        <a className="c-darktan" href="#" title="Darktan" />
                      </li>
                      <li>
                        <a className="c-grey" href="#" title="Grey" />
                      </li>
                      <li>
                        <a className="c-brown" href="#" title="Brown" />
                      </li>
                    </ul>
                    <h6 className="product-name">
                      <a href="product-details.html">Perfect Diamond Jewelry</a>
                    </h6>
                    <div className="price-box">
                      <span className="price-regular">$99.00</span>
                      <span className="price-old">
                        <del />
                      </span>
                    </div>
                  </div>
                </div>
                {/* product item end */}
                {/* product item start */}
                <div className="product-item">
                  <figure className="product-thumb">
                    <a href="product-details.html">
                      <img
                        className="pri-img"
                        src="assets/img/product/product-12.jpg"
                        alt="product"
                      />
                      <img
                        className="sec-img"
                        src="assets/img/product/product-9.jpg"
                        alt="product"
                      />
                    </a>
                    <div className="product-badge">
                      <div className="product-label new">
                        <span>sale</span>
                      </div>
                      <div className="product-label discount">
                        <span>15%</span>
                      </div>
                    </div>
                    <div className="button-group">
                      <a
                        href="wishlist.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to wishlist"
                      >
                        <i className="pe-7s-like" />
                      </a>
                      <a
                        href="compare.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to Compare"
                      >
                        <i className="pe-7s-refresh-2" />
                      </a>
                      <a href="#" data-toggle="modal" data-target="#quick_view">
                        <span
                          data-toggle="tooltip"
                          data-placement="left"
                          title="Quick View"
                        >
                          <i className="pe-7s-search" />
                        </span>
                      </a>
                    </div>
                    <div className="cart-hover">
                      <button className="btn btn-cart">add to cart</button>
                    </div>
                  </figure>
                  <div className="product-caption text-center">
                    <div className="product-identity">
                      <p className="manufacturer-name">
                        <a href="product-details.html">silver</a>
                      </p>
                    </div>
                    <ul className="color-categories">
                      <li>
                        <a
                          className="c-lightblue"
                          href="#"
                          title="LightSteelblue"
                        />
                      </li>
                      <li>
                        <a className="c-darktan" href="#" title="Darktan" />
                      </li>
                      <li>
                        <a className="c-grey" href="#" title="Grey" />
                      </li>
                      <li>
                        <a className="c-brown" href="#" title="Brown" />
                      </li>
                    </ul>
                    <h6 className="product-name">
                      <a href="product-details.html">
                        Diamond Exclusive Ornament
                      </a>
                    </h6>
                    <div className="price-box">
                      <span className="price-regular">$55.00</span>
                      <span className="price-old">
                        <del>$75.00</del>
                      </span>
                    </div>
                  </div>
                </div>
                {/* product item end */}
                {/* product item start */}
                <div className="product-item">
                  <figure className="product-thumb">
                    <a href="product-details.html">
                      <img
                        className="pri-img"
                        src="assets/img/product/product-7.jpg"
                        alt="product"
                      />
                      <img
                        className="sec-img"
                        src="assets/img/product/product-14.jpg"
                        alt="product"
                      />
                    </a>
                    <div className="product-badge">
                      <div className="product-label new">
                        <span>new</span>
                      </div>
                      <div className="product-label discount">
                        <span>20%</span>
                      </div>
                    </div>
                    <div className="button-group">
                      <a
                        href="wishlist.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to wishlist"
                      >
                        <i className="pe-7s-like" />
                      </a>
                      <a
                        href="compare.html"
                        data-toggle="tooltip"
                        data-placement="left"
                        title="Add to Compare"
                      >
                        <i className="pe-7s-refresh-2" />
                      </a>
                      <a href="#" data-toggle="modal" data-target="#quick_view">
                        <span
                          data-toggle="tooltip"
                          data-placement="left"
                          title="Quick View"
                        >
                          <i className="pe-7s-search" />
                        </span>
                      </a>
                    </div>
                    <div className="cart-hover">
                      <button className="btn btn-cart">add to cart</button>
                    </div>
                  </figure>
                  <div className="product-caption text-center">
                    <div className="product-identity">
                      <p className="manufacturer-name">
                        <a href="product-details.html">mony</a>
                      </p>
                    </div>
                    <ul className="color-categories">
                      <li>
                        <a
                          className="c-lightblue"
                          href="#"
                          title="LightSteelblue"
                        />
                      </li>
                      <li>
                        <a className="c-darktan" href="#" title="Darktan" />
                      </li>
                      <li>
                        <a className="c-grey" href="#" title="Grey" />
                      </li>
                      <li>
                        <a className="c-brown" href="#" title="Brown" />
                      </li>
                    </ul>
                    <h6 className="product-name">
                      <a href="product-details.html">Citygold Exclusive Ring</a>
                    </h6>
                    <div className="price-box">
                      <span className="price-regular">$60.00</span>
                      <span className="price-old">
                        <del>$70.00</del>
                      </span>
                    </div>
                  </div>
                </div>
                {/* product item end */}
              </div>
            </div>
          </div>
          {/* product tab content end */}
        </div>
      </div>
    </div>
  </div>
</section>
<div className="banner-statistics-area">
  <div className="container">
    <div className="row row-20 mtn-20">
      <div className="col-sm-6">
        <figure className="banner-statistics mt-20">
          <a href="#">
            <img src="img/banner/img1-top.jpg" alt="product banner" />
          </a>
          <div className="banner-content text-right">
            <h5 className="banner-text1">BEAUTIFUL</h5>
            <h2 className="banner-text2">
              Wedding<span>Rings</span>
            </h2>
            <a href="shop.html" className="btn btn-text">
              Shop Now
            </a>
          </div>
        </figure>
      </div>
      <div className="col-sm-6">
        <figure className="banner-statistics mt-20">
          <a href="#">
            <img src="img/banner/img2-top.jpg" alt="product banner" />
          </a>
          <div className="banner-content text-center">
            <h5 className="banner-text1">EARRINGS</h5>
            <h2 className="banner-text2">
              Tangerine Floral <span>Earring</span>
            </h2>
            <a href="shop.html" className="btn btn-text">
              Shop Now
            </a>
          </div>
        </figure>
      </div>
      <div className="col-sm-6">
        <figure className="banner-statistics mt-20">
          <a href="#">
            <img src="img/banner/img3-top.jpg" alt="product banner" />
          </a>
          <div className="banner-content text-center">
            <h5 className="banner-text1">NEW ARRIVALLS</h5>
            <h2 className="banner-text2">
              Pearl<span>Necklaces</span>
            </h2>
            <a href="shop.html" className="btn btn-text">
              Shop Now
            </a>
          </div>
        </figure>
      </div>
      <div className="col-sm-6">
        <figure className="banner-statistics mt-20">
          <a href="#">
            <img src="img/banner/img4-top.jpg" alt="product banner" />
          </a>
          <div className="banner-content text-right">
            <h5 className="banner-text1">NEW DESIGN</h5>
            <h2 className="banner-text2">
              Diamond<span>Jewelry</span>
            </h2>
            <a href="shop.html" className="btn btn-text">
              Shop Now
            </a>
          </div>
        </figure>
      </div>
    </div>
  </div>
</div>
<section
  className="testimonial-area section-padding bg-img"
  data-bg="assets/img/testimonial/testimonials-bg.jpg"
>
  <div className="container">
    <div className="row">
      <div className="col-12">
        {/* section title start */}
        <div className="section-title text-center">
          <h2 className="title">testimonials</h2>
          <p className="sub-title">What they say</p>
        </div>
        {/* section title start */}
      </div>
    </div>
    <div className="row">
      <div className="col-12">
        <div className="testimonial-thumb-wrapper">
          <div className="testimonial-thumb-carousel">
            <div className="testimonial-thumb">
              <img
                src="assets/img/testimonial/testimonial-1.png"
                alt="testimonial-thumb"
              />
            </div>
            <div className="testimonial-thumb">
              <img
                src="assets/img/testimonial/testimonial-2.png"
                alt="testimonial-thumb"
              />
            </div>
            <div className="testimonial-thumb">
              <img
                src="assets/img/testimonial/testimonial-3.png"
                alt="testimonial-thumb"
              />
            </div>
            <div className="testimonial-thumb">
              <img
                src="assets/img/testimonial/testimonial-2.png"
                alt="testimonial-thumb"
              />
            </div>
          </div>
        </div>
        <div className="testimonial-content-wrapper">
          <div className="testimonial-content-carousel">
            <div className="testimonial-content">
              <p>
                Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis
                tortor. Nunc scelerisque, nisi a blandit varius, nunc purus
                venenatis ligula, sed venenatis orci augue nec sapien. Cum
                sociis natoque
              </p>
              <div className="ratings">
                <span>
                  <i className="fa fa-star-o" />
                </span>
                <span>
                  <i className="fa fa-star-o" />
                </span>
                <span>
                  <i className="fa fa-star-o" />
                </span>
                <span>
                  <i className="fa fa-star-o" />
                </span>
                <span>
                  <i className="fa fa-star-o" />
                </span>
              </div>
              <h5 className="testimonial-author">lindsy niloms</h5>
            </div>
            <div className="testimonial-content">
              <p>
                Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis
                tortor. Nunc scelerisque, nisi a blandit varius, nunc purus
                venenatis ligula, sed venenatis orci augue nec sapien. Cum
                sociis natoque
              </p>
              <div className="ratings">
                <span>
                  <i className="fa fa-star-o" />
                </span>
                <span>
                  <i className="fa fa-star-o" />
                </span>
                <span>
                  <i className="fa fa-star-o" />
                </span>
                <span>
                  <i className="fa fa-star-o" />
                </span>
                <span>
                  <i className="fa fa-star-o" />
                </span>
              </div>
              <h5 className="testimonial-author">Daisy Millan</h5>
            </div>
            <div className="testimonial-content">
              <p>
                Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis
                tortor. Nunc scelerisque, nisi a blandit varius, nunc purus
                venenatis ligula, sed venenatis orci augue nec sapien. Cum
                sociis natoque
              </p>
              <div className="ratings">
                <span>
                  <i className="fa fa-star-o" />
                </span>
                <span>
                  <i className="fa fa-star-o" />
                </span>
                <span>
                  <i className="fa fa-star-o" />
                </span>
                <span>
                  <i className="fa fa-star-o" />
                </span>
                <span>
                  <i className="fa fa-star-o" />
                </span>
              </div>
              <h5 className="testimonial-author">Anamika lusy</h5>
            </div>
            <div className="testimonial-content">
              <p>
                Vivamus a lobortis ipsum, vel condimentum magna. Etiam id turpis
                tortor. Nunc scelerisque, nisi a blandit varius, nunc purus
                venenatis ligula, sed venenatis orci augue nec sapien. Cum
                sociis natoque
              </p>
              <div className="ratings">
                <span>
                  <i className="fa fa-star-o" />
                </span>
                <span>
                  <i className="fa fa-star-o" />
                </span>
                <span>
                  <i className="fa fa-star-o" />
                </span>
                <span>
                  <i className="fa fa-star-o" />
                </span>
                <span>
                  <i className="fa fa-star-o" />
                </span>
              </div>
              <h5 className="testimonial-author">Maria Mora</h5>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<footer className="footer-widget-area">
  <div className="footer-top section-padding">
    <div className="container">
      <div className="row">
        <div className="col-lg-3 col-md-6">
          <div className="widget-item">
            <div className="widget-title">
              <div className="widget-logo">
                <a href="index.html">
                  <img src="assets/img/logo/logo.png" alt="brand logo" />
                </a>
              </div>
            </div>
            <div className="widget-body">
              <p>
                We are a team of designers and developers that create high
                quality wordpress, shopify, Opencart{" "}
              </p>
            </div>
          </div>
        </div>
        <div className="col-lg-3 col-md-6">
          <div className="widget-item">
            <h6 className="widget-title">Contact Us</h6>
            <div className="widget-body">
              <address className="contact-block">
                <ul>
                  <li>
                    <i className="pe-7s-home" /> 4710-4890 Breckinridge USA
                  </li>
                  <li>
                    <i className="pe-7s-mail" />{" "}
                    <a href="mailto:demo@plazathemes.com">
                      demo@yourdomain.com{" "}
                    </a>
                  </li>
                  <li>
                    <i className="pe-7s-call" />{" "}
                    <a href="tel:(012)800456789987">(012) 800 456 789-987</a>
                  </li>
                </ul>
              </address>
            </div>
          </div>
        </div>
        <div className="col-lg-3 col-md-6">
          <div className="widget-item">
            <h6 className="widget-title">Information</h6>
            <div className="widget-body">
              <ul className="info-list">
                <li>
                  <a href="#">about us</a>
                </li>
                <li>
                  <a href="#">Delivery Information</a>
                </li>
                <li>
                  <a href="#">privet policy</a>
                </li>
                <li>
                  <a href="#">Terms &amp; Conditions</a>
                </li>
                <li>
                  <a href="#">contact us</a>
                </li>
                <li>
                  <a href="#">site map</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="col-lg-3 col-md-6">
          <div className="widget-item">
            <h6 className="widget-title">Follow Us</h6>
            <div className="widget-body social-link">
              <a href="#">
                <i className="fa fa-facebook" />
              </a>
              <a href="#">
                <i className="fa fa-twitter" />
              </a>
              <a href="#">
                <i className="fa fa-instagram" />
              </a>
              <a href="#">
                <i className="fa fa-youtube" />
              </a>
            </div>
          </div>
        </div>
      </div>
      <div className="row align-items-center mt-20">
        <div className="col-md-6">
          <div className="newsletter-wrapper">
            <h6 className="widget-title-text">Signup for newsletter</h6>
            <form className="newsletter-inner" id="mc-form">
              <input
                type="email"
                className="news-field"
                id="mc-email"
                autoComplete="off"
                placeholder="Enter your email address"
              />
              <button className="news-btn" id="mc-submit">
                Subscribe
              </button>
            </form>
            {/* mailchimp-alerts Start */}
            <div className="mailchimp-alerts">
              <div className="mailchimp-submitting" />
              {/* mailchimp-submitting end */}
              <div className="mailchimp-success" />
              {/* mailchimp-success end */}
              <div className="mailchimp-error" />
              {/* mailchimp-error end */}
            </div>
            {/* mailchimp-alerts end */}
          </div>
        </div>
        <div className="col-md-6">
          <div className="footer-payment">
            <img src="assets/img/payment.png" alt="payment method" />
          </div>
        </div>
      </div>
    </div>
  </div>
  <div className="footer-bottom">
    <div className="container">
      <div className="row">
        <div className="col-12">
          <div className="copyright-text text-center">
            <p>
              Powered By <a href="#">Corano</a>. Store 1 © 2019
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
{
  /* product-area start */
}
<div className="product-area section-ptb">
  <div className="container">
    <div className="row">
      <div className="col-lg-12">
        {/* section-title start */}
        <div className="section-title">
          <h2>New Arrivals</h2>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit</p>
        </div>
        {/* section-title end */}
      </div>
    </div>
    {/* product-warpper start */}
    <div className="product-warpper">
      <div className="row">
        <div className="col-lg-3 col-md-4 col-sm-6">
          {/* single-product-wrap start */}
          <div className="single-product-wrap">
            <div className="product-image">
              <a href="product-details.html">
                <img src="img/product/product-1.jpg" alt="" />
              </a>
              <div className="product-action">
                <a href="#" className="wishlist">
                  <i className="icon-heart" />
                </a>
                <a href="#" className="add-to-cart">
                  <i className="icon-handbag" />
                </a>
                <a
                  href="#"
                  className="quick-view"
                  data-toggle="modal"
                  data-target="#exampleModalCenter"
                >
                  <i className="icon-shuffle" />
                </a>
              </div>
            </div>
            <div className="product-content">
              <h3>
                <a href="product-details.html">Products Name 001</a>
              </h3>
              <div className="price-box">
                <span className="old-price">140.00</span>
                <span className="new-price">120.00</span>
              </div>
            </div>
          </div>
          {/* single-product-wrap end */}
        </div>
        <div className="col-lg-3 col-md-4 col-sm-6">
          {/* single-product-wrap start */}
          <div className="single-product-wrap">
            <div className="product-image">
              <a href="product-details.html">
              <img src="img/product/product-2.jpg" alt="" />
              </a>
              <div className="product-action">
                <a href="#" className="wishlist">
                  <i className="icon-heart" />
                </a>
                <a href="#" className="add-to-cart">
                  <i className="icon-handbag" />
                </a>
                <a
                  href="#"
                  className="quick-view"
                  data-toggle="modal"
                  data-target="#exampleModalCenter"
                >
                  <i className="icon-shuffle" />
                </a>
              </div>
            </div>
            <div className="product-content">
              <h3>
                <a href="product-details.html">Products Name 002</a>
              </h3>
              <div className="price-box">
                <span className="new-price">120.00</span>
              </div>
            </div>
          </div>
          {/* single-product-wrap end */}
        </div>
        <div className="col-lg-3 col-md-4 col-sm-6">
          {/* single-product-wrap start */}
          <div className="single-product-wrap">
            <div className="product-image">
              <a href="product-details.html">
              <img src="img/product/product-3.jpg" alt="" />
              </a>
              <div className="product-action">
                <a href="#" className="wishlist">
                  <i className="icon-heart" />
                </a>
                <a href="#" className="add-to-cart">
                  <i className="icon-handbag" />
                </a>
                <a
                  href="#"
                  className="quick-view"
                  data-toggle="modal"
                  data-target="#exampleModalCenter"
                >
                  <i className="icon-shuffle" />
                </a>
              </div>
            </div>
            <div className="product-content">
              <h3>
                <a href="product-details.html">Products Name 003</a>
              </h3>
              <div className="price-box">
                <span className="old-price">230.00</span>
                <span className="new-price">210.00</span>
              </div>
            </div>
          </div>
          {/* single-product-wrap end */}
        </div>
        <div className="col-lg-3 col-md-4 col-sm-6">
          {/* single-product-wrap start */}
          <div className="single-product-wrap">
            <div className="product-image">
              <a href="product-details.html">
              <img src="img/product/product-4.jpg" alt="" />
              </a>
              <div className="product-action">
                <a href="#" className="wishlist">
                  <i className="icon-heart" />
                </a>
                <a href="#" className="add-to-cart">
                  <i className="icon-handbag" />
                </a>
                <a
                  href="#"
                  className="quick-view"
                  data-toggle="modal"
                  data-target="#exampleModalCenter"
                >
                  <i className="icon-shuffle" />
                </a>
              </div>
            </div>
            <div className="product-content">
              <h3>
                <a href="product-details.html">Products Name 004</a>
              </h3>
              <div className="price-box">
                <span className="new-price">120.00</span>
              </div>
            </div>
          </div>
          {/* single-product-wrap end */}
        </div>
        <div className="col-lg-3 col-md-4 col-sm-6">
          {/* single-product-wrap start */}
          <div className="single-product-wrap">
            <div className="product-image">
              <a href="#">
              <img src="img/product/product-5.jpg" alt="" />
              </a>
              <div className="product-action">
                <a href="#" className="wishlist">
                  <i className="icon-heart" />
                </a>
                <a href="#" className="add-to-cart">
                  <i className="icon-handbag" />
                </a>
                <a
                  href="#"
                  className="quick-view"
                  data-toggle="modal"
                  data-target="#exampleModalCenter"
                >
                  <i className="icon-shuffle" />
                </a>
              </div>
            </div>
            <div className="product-content">
              <h3>
                <a href="product-details.html">Products Name 005</a>
              </h3>
              <div className="price-box">
                <span className="old-price">180.00</span>
                <span className="new-price">150.00</span>
              </div>
            </div>
          </div>
          {/* single-product-wrap end */}
        </div>
        <div className="col-lg-3 col-md-4 col-sm-6">
          {/* single-product-wrap start */}
          <div className="single-product-wrap">
            <div className="product-image">
              <a href="product-details.html">
              <img src="img/product/product-6.jpg" alt="" />
              </a>
              <div className="product-action">
                <a href="#" className="wishlist">
                  <i className="icon-heart" />
                </a>
                <a href="#" className="add-to-cart">
                  <i className="icon-handbag" />
                </a>
                <a
                  href="#"
                  className="quick-view"
                  data-toggle="modal"
                  data-target="#exampleModalCenter"
                >
                  <i className="icon-shuffle" />
                </a>
              </div>
            </div>
            <div className="product-content">
              <h3>
                <a href="product-details.html">Products Name 006</a>
              </h3>
              <div className="price-box">
                <span className="new-price">130.00</span>
              </div>
            </div>
          </div>
          {/* single-product-wrap end */}
        </div>
        <div className="col-lg-3 col-md-4 col-sm-6">
          {/* single-product-wrap start */}
          <div className="single-product-wrap">
            <div className="product-image">
              <a href="product-details.html">
              <img src="img/product/product-7.jpg" alt="" />
              </a>
              <div className="product-action">
                <a href="#" className="wishlist">
                  <i className="icon-heart" />
                </a>
                <a href="#" className="add-to-cart">
                  <i className="icon-handbag" />
                </a>
                <a
                  href="#"
                  className="quick-view"
                  data-toggle="modal"
                  data-target="#exampleModalCenter"
                >
                  <i className="icon-shuffle" />
                </a>
              </div>
            </div>
            <div className="product-content">
              <h3>
                <a href="product-details.html">Products Name 007</a>
              </h3>
              <div className="price-box">
                <span className="old-price">250.00</span>
                <span className="new-price">230.00</span>
              </div>
            </div>
          </div>
          {/* single-product-wrap end */}
        </div>
        <div className="col-lg-3 col-md-4 col-sm-6">
          {/* single-product-wrap start */}
          <div className="single-product-wrap">
            <div className="product-image">
              <a href="product-details.html">
              <img src="img/product/product-8.jpg" alt="" />
              </a>
              <div className="product-action">
                <a href="#" className="wishlist">
                  <i className="icon-heart" />
                </a>
                <a href="#" className="add-to-cart">
                  <i className="icon-handbag" />
                </a>
                <a
                  href="#"
                  className="quick-view"
                  data-toggle="modal"
                  data-target="#exampleModalCenter"
                >
                  <i className="icon-shuffle" />
                </a>
              </div>
            </div>
            <div className="product-content">
              <h3>
                <a href="product-details.html">Products Name 008</a>
              </h3>
              <div className="price-box">
                <span className="new-price">120.00</span>
              </div>
            </div>
          </div>
          {/* single-product-wrap end */}
        </div>
      </div>
    </div>
    {/* product-warpper start */}
  </div>
</div>
{
  /* product-area end */
}


    </div>
  );
}
}

export default App;
