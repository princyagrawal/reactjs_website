import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Link, BrowserRouter,Switch } from 'react-router-dom'
import './index.css';
import './App.css';
import App from './App';
import Login from './Login';
import Shop from './Shop';
import Product from './Product';
import Blog from './Blog';
import Contact from "./Contact";
import Wishlist from "./Wishlist";
import Account from "./Account";
import Cart from "./Cart";



const routs = (
  < BrowserRouter >
     <div>
     <header className="header-area header-wide bg-gray">
        {/* main header start */}
        <div className="main-header d-none d-lg-block">
          {/* header top start */}
          <div className="header-top bdr-bottom">
            <div className="container">
              <div className="row align-items-center">
                <div className="col-lg-6">
                  <div className="welcome-message">
                    <p>Welcome to Corano Jewellery online store</p>
                  </div>
                </div>
                <div className="col-lg-6 text-right">
                  <div className="header-top-settings">
                    <ul className="nav align-items-center justify-content-end">
                      <li className="curreny-wrap">
                        $ Currency
                        <i className="fa fa-angle-down" />
                        <ul className="dropdown-list curreny-list">
                          <li>
                            <a href="#">$ USD</a>
                          </li>
                          <li>
                            <a href="#">€ EURO</a>
                          </li>
                        </ul>
                      </li>
                      <li className="language">
                        <img src="img/icon/en.png" alt="flag" /> English
                        <i className="fa fa-angle-down" />
                        <ul className="dropdown-list">
                          <li>
                            <a href="#">
                              <img src="img/icon/en.png" alt="flag" /> english
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <img src="img/icon/fr.png" alt="flag" /> french
                            </a>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* header top end */}
          {/* header middle area start */}
          <div className="header-main-area sticky">
            <div className="container">
              <div className="row align-items-center position-relative">
                {/* start logo area */}
                <div className="col-lg-2">
                  <div className="logo">
                    <a href="index-2.html">
                      <img src="img/logo/logo.png" alt="brand logo" />
                    </a>
                  </div>
                </div>
                {/* start logo area */}
                {/* main menu area start */}
                <div className="col-lg-6 position-static">
                  <div className="main-menu-area">
                    <div className="main-menu">
                      {/* main menu navbar start */}
                      <nav className="desktop-menu">
                        <ul>
                          <li className="active">
                          <Link to="/">Home</Link>
                            {/*  <ul class="dropdown">
                                                    <li><a href="index.html">Home version 01</a></li>
                                                    <li><a href="index-2.html">Home version 02</a></li>
                                                    <li><a href="index-3.html">Home version 03</a></li>
                                                    <li><a href="index-4.html">Home version 04</a></li>
                                                    <li><a href="index-5.html">Home version 05</a></li>
                                                    <li><a href="index-6.html">Home version 06</a></li>
                                                </ul> */}
                          </li>
                          <li className="position-static">
                            <a href="#">
                              Megamenu
                              <i className="fa fa-angle-down" />
                            </a>
                            <ul className="megamenu dropdown">
                              <li className="mega-title">
                                <span>column 01</span>
                                <ul>
                                  <li>
                                  <Link to="/shop">shop grid left sidebar</Link>
                                    
                                  </li>
                                  <li>
                                  <Link to="/shop">shop grid right sidebar</Link>

                                
                                  </li>
                                  <li>
                                    <a href="#">
                                      shop list left sidebar
                                    </a>
                                  </li>
                                  <li>
                                    <a href="#">
                                      shop list right sidebar
                                    </a>
                                  </li>
                                </ul>
                              </li>
                              <li className="mega-title">
                                <span>column 02</span>
                                <ul>
                                  <li>
                                    <a href="#">product details</a>
                                  </li>
                                  <li>
                                    <a href="#">
                                      product details affiliate
                                    </a>
                                  </li>
                                  <li>
                                    <a href="#">
                                      product details variable
                                    </a>
                                  </li>
                                  <li>
                                    <a href="#">
                                      product details group
                                    </a>
                                  </li>
                                </ul>
                              </li>
                              <li className="mega-title">
                                <span>column 03</span>
                                <ul>
                                  <li>
                                    <a href="#">cart</a>
                                  </li>
                                  <li>
                                    <a href="#">checkout</a>
                                  </li>
                                  <li>
                                    <a href="#">compare</a>
                                  </li>
                                  <li>
                                    <a href="#">wishlist</a>
                                  </li>
                                </ul>
                              </li>
                              <li className="mega-title">
                                <span>column 04</span>
                                <ul>
                                  <li>
                                    <a href="#">my-account</a>
                                  </li>
                                  <li>
                                    <a href="#">login</a>
                                  </li>
                                  <li>
                                    <a href="#">about us</a>
                                  </li>
                                  <li>
                                    <a href="#">contact us</a>
                                  </li>
                                </ul>
                              </li>
                              <li className="megamenu-banners d-none d-lg-block">
                                <a href="product-details.html">
                                  <img
                                    src="img/banner/img1-static-menu.jpg"
                                    alt=""
                                  />
                                </a>
                              </li>
                              <li className="megamenu-banners d-none d-lg-block">
                                <a href="product-details.html">
                                  <img
                                    src="img/banner/img2-static-menu.jpg"
                                    alt=""
                                  />
                                </a>
                              </li>
                            </ul>
                          </li>
                          <li>
                          <Link to="/shop">
                              shop <i className="fa fa-angle-down" />
                            </Link>
                            <ul className="dropdown">
                            <li>
                          <Link to="/shop">
                            Shop</Link>
                          </li>
                          <li>
                          <Link to="/product">
                            ProductDetail</Link>
                          </li>
                            </ul>
                            </li>
                          <li>
                          <Link to="/blog">Blog</Link>
                          </li>
                          <li>
                          <Link to="/contact">Contact</Link>
                          </li>
                          
                  

                    
                        </ul>
                      </nav>
                      {/* main menu navbar end */}
                    </div>
                  </div>
                </div>
                {/* main menu area end */}
                {/* mini cart area start */}
                <div className="col-lg-4">
                  <div className="header-right d-flex align-items-center justify-content-xl-between justify-content-lg-end">
                    <div className="header-search-container">
                      <button className="search-trigger d-xl-none d-lg-block">
                        <i className="pe-7s-search" />
                      </button>
                      <form className="header-search-box d-lg-none d-xl-block animated jackInTheBox">
                        <input
                          type="text"
                          placeholder="Search entire store hire"
                          className="header-search-field bg-white"
                        />
                        <button className="header-search-btn">
                          <i className="pe-7s-search" />
                        </button>
                      </form>
                    </div>
                    <div className="header-configure-area">
                      <ul className="nav justify-content-end">
                        <li className="user-hover">
                          <a href="#">
                            <i className="pe-7s-user" />
                          </a>
                          <ul className="dropdown-list">
                          <li>
                          <Link to="/login">Login</Link>
                          </li>                              
                          <li>
                          <Link to="/login">Register</Link>
                          </li> 
                            <li>
                            <Link to="/account">Account</Link>
                            </li>
                          </ul>
                        </li>
                        <li>
                        <Link to="/wishlist"><i className="pe-7s-like" />
                            <div className="notification">0</div></Link>
                          
                        </li>
                        <li>
                        <Link to="/cart"> <i className="pe-7s-shopbag" />
                            <div className="notification">2</div></Link>
                          
                        </li>
                      </ul>

                    </div>
                  </div>
                </div>
                {/* mini cart area end */}
              </div>
            </div>
          </div>
          {/* header middle area end */}
        </div>
        {/* main header start */}
        {/* mobile header start */}
        {/* mobile header start */}
        <div className="mobile-header d-lg-none d-md-block sticky">
          {/*mobile header top start */}
          <div className="container-fluid">
            <div className="row align-items-center">
              <div className="col-12">
                <div className="mobile-main-header">
                  <div className="mobile-logo">
                    <a href="index.html">
                      <img src="img/logo/logo.png" alt="Brand Logo" />
                    </a>
                  </div>
                  <div className="mobile-menu-toggler">
                    <div className="mini-cart-wrap">
                      <a href="cart.html">
                        <i className="pe-7s-shopbag" />
                        <div className="notification">0</div>
                      </a>
                    </div>
                    <button className="mobile-menu-btn">
                      <span />
                      <span />
                      <span />
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* mobile header top start */}
        </div>
        {/* mobile header end */}
        {/* mobile header end */}
        {/* offcanvas mobile menu start */}
        {/* off-canvas menu start */}
        <aside className="off-canvas-wrapper">
          <div className="off-canvas-overlay" />
          <div className="off-canvas-inner-content">
            <div className="btn-close-off-canvas">
              <i className="pe-7s-close" />
            </div>
            <div className="off-canvas-inner">
              {/* search box start */}
              <div className="search-box-offcanvas">
                <form>
                  <input type="text" placeholder="Search Here..." />
                  <button className="search-btn">
                    <i className="pe-7s-search" />
                  </button>
                </form>
              </div>
              {/* search box end */}
              {/* mobile menu start */}
              <div className="mobile-navigation">
                {/* mobile menu navigation start */}
                <nav>
                  <ul className="mobile-menu">
                    <li className="menu-item-has-children">
                      <a href="index.html">Home</a>
                      <ul className="dropdown">
                        <li>
                          <a href="index.html">Home version 01</a>
                        </li>
                        <li>
                          <a href="index-2.html">Home version 02</a>
                        </li>
                        <li>
                          <a href="index-3.html">Home version 03</a>
                        </li>
                        <li>
                          <a href="index-4.html">Home version 04</a>
                        </li>
                        <li>
                          <a href="index-5.html">Home version 05</a>
                        </li>
                        <li>
                          <a href="index-6.html">Home version 06</a>
                        </li>
                      </ul>
                    </li>
                    <li className="menu-item-has-children">
                      <a href="#">MegaMenu</a>
                      <ul className="megamenu dropdown">
                        <li className="mega-title menu-item-has-children">
                          <a href="#">column 01</a>
                          <ul className="dropdown">
                            <li>
                              <a href="shop.html">shop grid left sidebar</a>
                            </li>

                            <li>
                              <a href="shop-grid-right-sidebar.html">
                                shop grid right sidebar
                              </a>
                            </li>
                            <li>
                              <a href="shop-list-left-sidebar.html">
                                shop list left sidebar
                              </a>
                            </li>
                            <li>
                              <a href="shop-list-right-sidebar.html">
                                shop list right sidebar
                              </a>
                            </li>
                          </ul>
                        </li>
                        <li className="mega-title menu-item-has-children">
                          <a href="#">column 02</a>
                          <ul className="dropdown">
                            <li>
                              <a href="product-details.html">product details</a>
                            </li>
                            <li>
                              <a href="product-details-affiliate.html">
                                product details affiliate
                              </a>
                            </li>
                            <li>
                              <a href="product-details-variable.html">
                                product details variable
                              </a>
                            </li>
                            <li>
                              <a href="product-details-group.html">
                                product details group
                              </a>
                            </li>
                          </ul>
                        </li>
                        <li className="mega-title menu-item-has-children">
                          <a href="#">column 03</a>
                          <ul className="dropdown">
                            <li>
                              <a href="cart.html">cart</a>
                            </li>
                            <li>
                              <a href="checkout.html">checkout</a>
                            </li>
                            <li>
                              <a href="compare.html">compare</a>
                            </li>
                            <li>
                              <a href="wishlist.html">wishlist</a>
                            </li>
                          </ul>
                        </li>
                        <li className="mega-title menu-item-has-children">
                          <a href="#">column 04</a>
                          <ul className="dropdown">
                            <li>
                              <a href="my-account.html">my-account</a>
                            </li>
                            <li>
                          <Link to="/login">Login-Register</Link>
                          </li> 
                            <li>
                              <a href="about-us.html">about us</a>
                            </li>
                            <li>
                              <a href="contact-us.html">contact us</a>
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </li>
                    <li className="menu-item-has-children ">
                      <a href="#">shop</a>
                      <ul className="dropdown">
                        <li className="menu-item-has-children">
                          <a href="#">shop grid layout</a>
                          <ul className="dropdown">
                            <li>
                              <a href="shop.html">shop grid left sidebar</a>
                            </li>
                            <li>
                          <Link to="/shop">Shop</Link>
                          </li> 
                            <li>
                              <a href="shop-grid-right-sidebar.html">
                                shop grid right sidebar
                              </a>
                            </li>
                            <li>
                              <a href="shop-grid-full-3-col.html">
                                shop grid full 3 col
                              </a>
                            </li>
                            <li>
                              <a href="shop-grid-full-4-col.html">
                                shop grid full 4 col
                              </a>
                            </li>
                          </ul>
                        </li>
                        <li className="menu-item-has-children">
                          <a href="#">shop list layout</a>
                          <ul className="dropdown">
                            <li>
                              <a href="shop-list-left-sidebar.html">
                                shop list left sidebar
                              </a>
                            </li>
                            <li>
                              <a href="shop-list-right-sidebar.html">
                                shop list right sidebar
                              </a>
                            </li>
                            <li>
                              <a href="shop-list-full-width.html">
                                shop list full width
                              </a>
                            </li>
                          </ul>
                        </li>
                        <li className="menu-item-has-children">
                          <a href="#">products details</a>
                          <ul className="dropdown">
                            <li>
                              <a href="product-details.html">product details</a>
                            </li>
                            <li>
                              <a href="product-details-affiliate.html">
                                product details affiliate
                              </a>
                            </li>
                            <li>
                              <a href="product-details-variable.html">
                                product details variable
                              </a>
                            </li>
                            <li>
                              <a href="product-details-group.html">
                                product details group
                              </a>
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </li>
                    <li className="menu-item-has-children ">
                      <a href="#">Blog</a>
                      <ul className="dropdown">
                        <li>
                          <a href="blog-left-sidebar.html">blog left sidebar</a>
                        </li>
                        <li>
                          <a href="blog-list-left-sidebar.html">
                            blog list left sidebar
                          </a>
                        </li>
                        <li>
                          <a href="blog-right-sidebar.html">blog right sidebar</a>
                        </li>
                        <li>
                          <a href="blog-list-right-sidebar.html">
                            blog list right sidebar
                          </a>
                        </li>
                        <li>
                          <a href="blog-grid-full-width.html">blog grid full width</a>
                        </li>
                        <li>
                          <a href="blog-details.html">blog details</a>
                        </li>
                        <li>
                          <a href="blog-details-left-sidebar.html">
                            blog details left sidebar
                          </a>
                        </li>
                        <li>
                          <a href="blog-details-audio.html">blog details audio</a>
                        </li>
                        <li>
                          <a href="blog-details-video.html">blog details video</a>
                        </li>
                        <li>
                          <a href="blog-details-image.html">blog details image</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="contact-us.html">Contact us</a>
                    </li>
                  </ul>
                </nav>
                {/* mobile menu navigation end */}
              </div>
              {/* mobile menu end */}
              <div className="mobile-settings">
                <ul className="nav">
                  <li>
                    <div className="dropdown mobile-top-dropdown">
                      <a
                        href="#"
                        className="dropdown-toggle"
                        id="currency"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                      >
                        Currency
                        <i className="fa fa-angle-down" />
                      </a>
                      <div className="dropdown-menu" aria-labelledby="currency">
                        <a className="dropdown-item" href="#">
                          $ USD
                        </a>
                        <a className="dropdown-item" href="#">
                          $ EURO
                        </a>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div className="dropdown mobile-top-dropdown">
                      <a
                        href="#"
                        className="dropdown-toggle"
                        id="myaccount"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                      >
                        My Account
                        <i className="fa fa-angle-down" />
                      </a>
                      <div className="dropdown-menu" aria-labelledby="myaccount">
                        <a className="dropdown-item" href="my-account.html">
                          my account
                        </a>
                        <a className="dropdown-item" href="login-register.html">
                         
                          login
                        </a>
                        <a className="dropdown-item" href="login-register.html">
                          register
                        </a>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
              {/* offcanvas widget area start */}
              <div className="offcanvas-widget-area">
                <div className="off-canvas-contact-widget">
                  <ul>
                    <li>
                      <i className="fa fa-mobile" />
                      <a href="#">0123456789</a>
                    </li>
                    <li>
                      <i className="fa fa-envelope-o" />
                      <a href="#">info@yourdomain.com</a>
                    </li>
                  </ul>
                </div>
                <div className="off-canvas-social-widget">
                  <a href="#">
                    <i className="fa fa-facebook" />
                  </a>
                  <a href="#">
                    <i className="fa fa-twitter" />
                  </a>
                  <a href="#">
                    <i className="fa fa-pinterest-p" />
                  </a>
                  <a href="#">
                    <i className="fa fa-linkedin" />
                  </a>
                  <a href="#">
                    <i className="fa fa-youtube-play" />
                  </a>
                </div>
              </div>
              {/* offcanvas widget area end */}
            </div>
          </div>
        </aside>
        {/* off-canvas menu end */}
        {/* offcanvas mobile menu end */}
      </header>
        
        <Switch>
        <Route exact path="/" component={App} />
        <Route path="/login" component={Login} />
        <Route path="/shop" component={Shop} />
        <Route path="/product" component={Product} />
        <Route path="/blog" component={Blog} />
        <Route path="/contact" component={Contact} />
        <Route path="/wishlist" component={Wishlist} />
        <Route path="/cart" component={Cart} />
        <Route path="/account" component={Account} />






        

         </Switch>
  
     </div>
  </ BrowserRouter >
);
ReactDOM.render(routs, document.getElementById('root'))

// ReactDOM.render(
//   <React.StrictMode>
//     <App />,
//     <Login />

//   </React.StrictMode>,
//   document.getElementById('root')
// );

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
